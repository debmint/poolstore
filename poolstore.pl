#! /usr/bin/perl -w

# Pool Store manager program to keep a record on pool sales

use strict;
use warnings;
use Gtk3 -init;
use Glib qw/TRUE FALSE/;
use DBI;
use DBD::Pg;
use Data::Dumper;

# Set up GObject-Introspection for GtkCalendar and Styleprint
use Glib::Object::Introspection;

Glib::Object::Introspection->setup(
    basename => 'Gio',
    version => '2.0',
    package => 'Gio2');

Glib::Object::Introspection->setup (
    basename => 'GtkCalendar',
    version => '1.0',
    package => 'GtkCalendar1',
    search_path => '/usr/local/lib');

Glib::Object::Introspection->setup (
    basename => 'StylePrint',
    version => '1.0',
    package => 'StylePrint1',
    search_path => '/usr/local/lib');

our $w_main;     # The main, toplevel window
my $builder;
my $newsalepkg;
our $dbh;        # The DBI::Pg handle
my $sp_connstr;

BEGIN {
package NewPoolSale;
{
    use Data::Dumper;
    sub new {
        my $class = shift;

        my $self = {};

        $self->{bldr} = Gtk3::Builder->new;
        my $bldr = $self->{bldr};
        $bldr->add_from_file ("glade/newpoolsale.glade");
        $self->{dlg} = $bldr->get_object ('new_pool_sale_dialog');
        $self->{dlg}->set_transient_for ($w_main);

        # Insert the GtkCalendarEntry into its place

        $self->{obj}->{date} = GtkCalendar1::Entry->new;
        $bldr->get_object('grid-poolsale')->attach (
                    $self->{obj}->{date}, 1, 0, 1, 1);

        populate_poolnames ($self);
        populate_lastnames ($self);

        # Set up hashes to correlate widgets to poolsales/customertable tables
        $self->{poolsales}->{saledate} = $self->{obj}->{date};

        foreach my $id (qw/ns-status ns-poolid ns-p_comment/) {
            my $widg = $bldr->get_object ($id);
            my $id2 = $id;
            $id2 =~ s/^ns-//;
            $self->{poolsales}->{$id2} = $widg;
        }

        foreach my $id (qw/ns-firstname ns-lastname ns-mi ns-streetaddr
                    ns-city ns-state ns-zip ns-phone ns-cellphone/) {
            my $widg = $bldr->get_object ($id);
            my $id2 = $id;
            $id2 =~ s/^ns-//;
            $self->{customertable}->{$id2} = $widg;
        }

        # Connect some signals that couldn't be set up in glade file
        # since $self is passed as the user_data parameter.

        $bldr->get_object ('ns-zip')->signal_connect_swapped(
            "changed" =>\&on_ns_zip_changed, $self);
        $bldr->get_object ('ns-firstname')->signal_connect_swapped (
            'enter-notify-event' => \&on_ns_firstname_enter_notify_event,
            $self);
        $bldr->get_object ('ns-btn-save')->signal_connect_swapped (
            'clicked' => \&on_ns_btn_save_clicked, $self);
        $bldr->get_object ('ns-btn-reset')->signal_connect_swapped (
           'clicked' => \&on_ns_btn_reset_clicked, $self);

        bless $self,$class;
        return $self;
    }

    sub on_ns_btn_reset_clicked {
        my $self = shift;

        foreach my $id (qw/ns-p_comment ns-firstname ns-lastname ns-mi
                    ns-streetaddr ns-city ns-state ns-zip
                    ns-phone ns-cellphone/) {
            $self->{bldr}->get_object($id)->set_text('');
        }
    }

    sub run {
        my $self = shift;

        on_ns_btn_reset_clicked ($self);
        $self->{dlg}->run;
        $self->{dlg}->hide;
    }

    sub populate_lastnames {
        my $self = shift;
        my $qry =
            "SELECT DISTINCT lastname FROM customertable ORDER BY lastname";
        my $store = $self->{bldr}->get_object('liststore-lastname');

        if ($store->get_iter_first) {
            $store->clear;
        }

        my $names = $dbh->selectcol_arrayref ($qry);

        if ($names) {
            foreach my $row (@$names) {
                $store->set ($store->append, 0, $row);
            }
        }
    }

    # Populate the Pool ComboBox

    sub populate_poolnames {
        my $self = shift;

        my $qry = qq/SELECT poolid::text,poolsize,poolname,/ .
                    qq/concat_ws(' ',poolsize,poolname) / .
                    qq/FROM poolnames WHERE is_active IS TRUE / .
                    qq/ORDER BY pooltype DESC,disp_order/;
        my $rslt = $dbh->selectall_arrayref ($qry);
        my $store = $self->{bldr}->get_object('nps-poolnames-liststore');

        if (my $it = $store->get_iter_first) {
            $store->clear;
        }

        foreach my $row (@$rslt) {
            my $itr = $store->append;

            for (0 .. scalar(@$row) - 1) {
                $store->set ($itr, $_, $row->[$_]);
            }
        }
    }

    sub on_ns_zip_changed {
        my ($self, $ent) = @_;
        my $curzip = $ent->get_text;

        $self->{cust_id} = 0;

        if (length ($curzip) == 5) {
            my $qry = 'SELECT city,state FROM zz_zipcodes WHERE zip = ?';
            my $cty = $dbh->selectrow_arrayref ($qry, undef, $ent->get_text);

            if ($dbh->err) {
                print "Error\n" . $dbh->errstr;
            }

            if ($cty) {
                #print "Found {$cty->[0]}, ${$cty->[1]}\n";
                $self->{bldr}->get_object('ns-city')->set_text ($cty->[0]);
                $self->{bldr}->get_object('ns-state')->set_text ($cty->[1]);
            }

            # Check to see if this customer is already in the database

            my $custlist = $dbh->selectall_arrayref(
                "SELECT cust_id,streetaddr FROM customertable " .
                "WHERE firstname=? and lastname=? and zip=?", undef,
                $self->{bldr}->get_object('ns-firstname')->get_text,
                $self->{bldr}->get_object('ns-lastname')->get_text, $curzip);

            if ($custlist and scalar @$custlist) {
                my $resp;
                my $addr;

                foreach my $cust (@$custlist) {
                    $addr = $cust->[1];
                    my $dlg = Gtk3::MessageDialog->new ($self->{dlg},
                        'destroy-with-parent',
                        'question',
                        'yes-no',
                        "Is Street address '%s' ?", $addr);
                    $resp = $dlg->run;
                    $dlg->destroy;

                    if ($resp eq 'yes') {
                        $self->{bldr}->get_object('ns-streetaddr')->set_text (
                            $addr);
                        $self->{cust_id} = $cust->[0];
                        last;
                    }
                }
            }
        }
    }

    sub build_insert_query {
        my ($tblnam, $widgets, $vals) = @_;

        my (@cols, @placeholdr);

        # We pass (colname,cust_id) in @$vals if we're doing the pool part

        if (scalar @$vals) {
            push @cols, shift (@$vals);
            push @placeholdr, '?';
        }

        while (my ($col, $widg) = each (%$widgets)) {
            my $value = undef;

            if (ref ($widg) eq 'Gtk3::RadioButton') {
                my $grp = $widg->get_group;

                foreach (@$grp) {
                    if ($_->get_active) {
                        $value = $_->get_label;
                        last;
                    }
                }
            }
            elsif (ref ($widg) eq 'Gtk3::ComboBox') {
                $value = $widg->get_active_id;
            }
            else {
                $value = $widg->get_text;
            }

            if ($value) {
                push @cols, $col;
                push @$vals, $value;
                push @placeholdr, '?';
            }
        }

        return sprintf ("INSERT INTO %s (%s) VALUES(%s)",
            $tblnam, join(',',@cols), join (',', @placeholdr));
    }

    # ============================================================= #
    # Callback for when the New Pool Sale "Save" button is clicked  #
    # Retrieves the data from the controls and inserts it into the  #
    # database                                                      #
    # ============================================================= #

    sub on_ns_btn_save_clicked {
        my ($self,$btn) = @_;

        my (@p_vals, @c_vals);

        unless ($self->{cust_id}) {
            my $custqry = build_insert_query ('customertable',
                        $self->{customertable}, \@c_vals);
            $custqry .= ' RETURNING cust_id';
            my $id = $dbh->selectrow_arrayref ($custqry, undef, @c_vals);
            $self->{cust_id} = $id->[0];
            if ($dbh->err) {
                print $dbh->errstr;
            }
        }

        @p_vals = ('cust_id', $self->{cust_id});
        my $poolqry = build_insert_query ('poolsales', $self->{poolsales},
            \@p_vals);
        my $rslt = $dbh->do($poolqry, undef, @p_vals);

        unless ($rslt) {
            print $dbh->errstr;
        }
    }

    sub on_ns_firstname_enter_notify_event {
        my ($self, $event, $ent) = @_;

        my $ln = $self->{bldr}->get_object ('ns-lastname')->get_text;

        if ($ln and length($ln)) {
            my $qry = "SELECT DISTINCT firstname from customertable " .
                    "WHERE lower(lastname) = lower(?) ORDER BY firstname";
            my $firsts = $dbh->selectcol_arrayref ($qry, undef, $ln);

            if ($firsts and scalar(@$firsts)) {
                my $store = $self->{bldr}->get_object ('liststore-firstname');

                if ($store->get_iter_first) {
                    $store->clear;
                }

                foreach (@$firsts) {
                    $store->set ($store->append, 0, $_);
                }
            }
        }
    }
}

package CustomerSearch;
{
    use Data::Dumper;

    # ============================================================= #
    # Set up customer search functionality                          #
    # ============================================================= #

    sub new {
        my $class = shift;

        my $self = {};

        # Save widgets to $customer_search hash ref
        $self->{builder} = Gtk3::Builder->new();
        $self->{builder}->add_from_file ('glade/poolcustomersearch.glade');
        $self->{dialog} = $self->{builder}->get_object ('dlg_cust_srch');
        $self->{dialog}->set_transient_for ($w_main);
        $self->{builder}->get_object (
            'dlg_customer_view_multi')->set_transient_for ($w_main);
        $self->{builder}->connect_signals;
        $self->{buttons}->{phone} = $self->{builder}->get_object (
                    'rb-cust-phone');
        $self->{buttons}->{phone}->signal_connect (
            'clicked'=>\&on_rb_cust_phone_clicked, $self);
        $self->{buttons}->{name} = $self->{builder}->get_object (
                    'rb-cust-name');
        $self->{buttons}->{name}->signal_connect (
                    'clicked' => \&on_rb_cust_name_clicked, $self);
        $self->{names}->{last} = $self->{builder}->get_object ('cs_lastname');
        $self->{names}->{first} = $self->{builder}->get_object (
                    'cs_firstname');
        $self->{names}->{mi} = $self->{builder}->get_object ('cs_mi');
        $self->{phone} = $self->{builder}->get_object ('cs-phone-num');
    #    $customer_search->{'cs-lastname-ec'} = $builder->get_object ('cust-lastname-entrycompletion');
    #    $customer_search->{'cs-firstname-ec'} = $builder->get_object ('cust-firstname-entrycompletion');

        $self->{builder}->get_object ('cs-btn-srch')->signal_connect (
           'clicked' =>\&on_cs_btn_srch_clicked, $self);

        # Now set up entry completion...
        $self->{liststore_lastname} = Gtk3::ListStore->new("Glib::String");
        my $comp = $self->{names}->{last}->get_completion;
        $comp->set_model($self->{liststore_lastname});
        $comp->set_text_column(0);
        $comp = $self->{names}->{first}->get_completion;
        $self->{liststore_firstname} = Gtk3::ListStore->new("Glib::String");
        $comp->set_model($self->{liststore_firstname});
        $comp->set_text_column(0);
        my $flds = "lastname";

        foreach my $t ($self->{names}->{last}, $self->{names}->{first}) {
            my $l = $t->get_completion->get_model;
            my $q = "SELECT DISTINCT $flds as name FROM customertable ORDER BY name";
            my $r = $dbh->selectcol_arrayref($q);

            foreach (@$r) { $l->set ($l->append, 0, $_) }
            $flds = qq/firstname/;
        }

        bless $self,$class;
        return $self;
    }

    sub rb_cust_phone_enable {
        my $self = shift;

        $self->{phone}->set_sensitive (1);

        foreach (keys %{$self->{names}}) {
            $self->{names}->{$_}->set_sensitive(0);
        }
    }

    sub rb_cust_name_enable {
        my $self = shift;

        $self->{phone}->set_sensitive (0);

        foreach (keys %{$self->{names}}) {
            $self->{names}->{$_}->set_sensitive(1);
        }
    }

    # ============================================================= #
    # Callback for when the customer search firstname entry         #
    # acquires focus.  It refills the completion store with         #
    # the firstnames that match the lastname, if it is available    #
    # ============================================================= #

    sub on_cs_firstname_enter_notify_event {
        my ($me,$direction,$ln) = @_;

        my $lastnm = $ln->get_text;

        unless ($lastnm) {return}
        unless (length($lastnm) > 0) {return}

        my $qry = qq/SELECT distinct firstname FROM customertable / .
        qq/WHERE lastname ILIKE ? ORDER BY firstname/;
        my $rslt = $dbh->selectcol_arrayref($qry,undef,$lastnm);

        if ($rslt) {
            my $lstore = $me->get_completion->get_model;
            $lstore->clear;

            foreach my $na (@$rslt) {
                unless (defined $na) {
                    print "\nUndefined value in result\n";last}
                $lstore->set ($lstore->append, 0, $na);
            }
        }
    }

    sub on_rb_cust_phone_clicked {
        my ($btn, $obj) = @_;

        if ($btn->get_active) {
            $obj->rb_cust_phone_enable;
        }
        else {
            $obj->rb_cust_name_enable;
        }
    }

    sub on_rb_cust_name_clicked {
        my ($btn, $obj) = @_;
        if ($_[0]->get_active) {
            $obj->rb_cust_name_enable;
        }
        else {
            $obj->rb_cust_phone_enable;
        }
    }

    # ============================================================= #
    # Callback for when the OK button is clicked.  It provides the  #
    # id for the selected customer and then calls                   #
    # display_customer_with_pools() function                        #
    # ============================================================= #

    sub on_cust_view_multi_button_ok_clicked {
        my ($okbtn, $treeview) = @_;
        my $iter = $treeview->get_selection->get_selected;

        display_customer_with_pools($treeview->get_model->get($iter, 7));
    }

    sub select_cust_from_ids {
        my ($self, $ids) = @_;

        my $dlg = $self->{builder}->get_object ('dlg_customer_view_multi');
        my $view = $self->{builder}->get_object ('treeview_customer_data');

        # First build a list of placeholders for the query
        my @placeholders;

        foreach (@$ids) {
            push @placeholders, 'cust_id = ?';
        }

        my $qry = qq/SELECT lastname,firstname,coalesce(mi,''),streetaddr,/ .
        qq/city,state,zip,cust_id FROM poolsalesfullview WHERE / .
        join (' OR ', @placeholders);

        my $custs = $dbh->selectall_arrayref ($qry, undef, @$ids);

        unless ($custs) {
            my $msg = 'No data was retrieved...';

            if ($dbh->err) {
                $msg .= "\n" . $dbh->errstr;
            }

            my $d = Gtk3::MessageDialog->new ($w_main, 'destroy-with-parent',
                'notice', 'gtk-ok', $msg);
            $d->run;
            $d->destroy;
            return;
        }

        $view->get_model->clear;

        foreach my $row (@$custs) {
            my $iter = $view->get_model->append();

            unless ($view->get_model->iter_is_valid($iter)) {
                my $d = MessageDialog->new ($dlg, 'destroy-with-parent',
                    'warning', 'gtk-close',
                    'Invalid iter for TreeView');
                return;
            }

            my (@cols, @vals);
            my @parms = ($iter);

            for (my $x = 0; $x < scalar (@$row); $x++) {
                #push(@cols, $x); push (@vals, $row->[$x]);
                push (@parms, $x => $row->[$x]);
            }

            #$view->get_model->set ($iter, \@cols, \@vals);
            $view->get_model->set(@parms);
        }

    #     ------ Testing ------
    #    my $it = $view->get_model->get_iter_first;
    #
    #    while ($it) {
    #        my ($l,$f);
    #        $l = $view->get_model->get_value($it, 0);
    #        $f = $view->get_model->get_value($it, 1);
    #        print "$f $l\n";
    #
    #        unless ($view->get_model->iter_next($it)) {
    #            $it = undef;
    #        }
    #    } print "---------\n";
    #     ------ Testing ------

        $dlg->run;
        $dlg->hide;
    #    return 1;
    }

    # ============================================================= #
    # Callback for when customer search "Search" button is          #
    # clicked.  Checks whether searching by phone # or name         #
    # then performs database search for customer.                   #
    # ============================================================= #

    sub on_cs_btn_srch_clicked {
        my ($btn, $self) = @_;
        #my $qry = "SELECT lastname,firstname,streetaddr,city,state,zip,cust_id " .
        #"FROM customertable WHERE ";
        my $qry = "SELECT cust_id FROM customertable WHERE ";
        my @binds;

        if ($self->{phone}->get_sensitive) {
            my $srchnum = $self->{phone}->get_text;

            unless (length($srchnum) >=4) {
                my $d = MessageDialog->new ($w_main, 'destroy-with-parent',
                    'warning', 'gtk-close',
                    'Please enter at least 4 last of the last digits of number');
                $d->run;
                $d->destroy;
                return;
            }

            $qry .= "phone LIKE ? OR cell LIKE ?";
            @binds = ('%' . $srchnum, '%' . $srchnum);
        }
        else {
            my $lastname = $self->{names}->{last}->get_text;
            my $firstname = $self->{names}->{first}->get_text;
            my @flds;

            if ($lastname) {
                if (length($lastname)) {
                    push @binds, $lastname;
                    push @flds, 'lastname ILIKE ?';
                }
            }

            if (length($firstname)) {
                push @binds, $firstname;
                push @flds, 'firstname ILIKE ?'
            }

            $qry .= join (' AND ', @flds);
        }

        my $rslt = $dbh->selectcol_arrayref ($qry, undef, @binds);

        my $custcount = scalar @$rslt;

        if ($custcount == 0) {
            my $md = Gtk3::MessageDialog->new ($w_main, 'destroy-with-parent',
                'warning', 'ok',
                "No Customers with provided criteria were found\n" .
                "Returning to try again...");
            $md->run;
            $md->destroy;
            return;
        }
        elsif ($custcount == 1) {
            display_customer_with_pools($rslt->[0])
        }
        else {
            if (my $id = select_cust_from_ids($self, $rslt)) {
                display_customer_with_pools($id)
            }
        }
    }

    sub display_customer_with_pools {
        my $id = shift;

        my $qry = "SELECT fullname(firstname,mi,lastname)," .
        "streetaddr,city,state,zip,cust_id FROM customertable WHERE cust_id=?";
        my $cust = $dbh->selectrow_arrayref($qry, undef, $id);

        if ($cust) {
            my $poolsqry = "SELECT concat_ws(' ',poolsize,poolname) AS pool," .
                "saledate " .
                "FROM (SELECT * FROM poolsales WHERE cust_id =?) a " .
                "LEFT JOIN poolnames USING (poolid) ORDER BY pool";
            my $pools = $dbh->selectall_arrayref($poolsqry,undef,$id);

            if ($dbh->err) {
                my $md = Gtk3::MessageDialog->new($w_main,
                    'destroy-with-parent','error','close',
                    $dbh->errstr);
                return;
            }

            unless ($pools) { return }

            my $dialog = Gtk3::Dialog->new("Customer Info", $w_main,
                'destroy-with-parent',
                'gtk-close' => 'ok');
            $dialog->get_content_area->set_spacing(10);
            $dialog->get_content_area->set_border_width(10);
            my $frame = Gtk3::Frame->new("Customer");
            $frame->set_label_align(0.10,0.50);
            my $grid = Gtk3::Grid->new();
            $grid->set_row_spacing (5);
            $grid->set_border_width(5);
            $grid->set_column_spacing (20);
            my @lbls = ('Name:','Address:','City:','State:','Zip:');

            my $x;

            # Print Customer information
            for ( $x = 0; $x < scalar (@lbls); $x++) {
                my $lbl;
                $lbl = Gtk3::Label->new ($lbls[$x]);
                $lbl->set_halign('end');
                $grid->attach ($lbl, 1, $x + 1, 1, 1);
                $lbl = Gtk3::Label->new($cust->[$x]);
                $lbl->set_halign( 'start');
                $grid->attach($lbl, 2, $x + 1, 1, 1);
            }

            $frame->add($grid);
            $dialog->get_content_area->add($frame);

            # Print pool(s) sold to customer
            $frame = Gtk3::Frame->new("Pools");
            $frame->set_label_align(0.10,0.50);
            $grid = Gtk3::Grid->new;
            $grid->set_column_spacing(5);
            $grid->set_border_width(5);

            foreach (@$pools) {
                my $lbl = Gtk3::Label->new($_->[0]);
                $lbl->set_halign('start');
                $grid->attach ($lbl, 1, $x, 1, 1);
                $grid->attach(Gtk3::Label->new(' - '), 2, $x, 1, 1);
                $grid->attach (Gtk3::Label->new($_->[1]), 3, $x++, 1, 1);
            }

            $frame->add($grid);
            $dialog->get_content_area->add($frame);
            $dialog->show_all;
            $dialog->run;
            $dialog->destroy;
        }
    }
}
}

sub kill_wmain {
    Gtk3::main_quit;
}

# ============================================================= #
# Dialog window to select a year                                #
# ============================================================= #

my $cur_yr;     # The year selected
my $yr_sel;     # The year-selection dialog after it is created
my $customer_search; # Customer search instance

# ============================================================= #
# Callback for when the OK Button is clicked.  It sets the      #
# variable $cur_yr to the year selected.                        #
# ============================================================= #

sub sel_yr_ret {
    my ($resp,$yr) = @_;

    $cur_yr = $yr->get_value_as_int;
}

# ============================================================= #
# Handles initial setup of widgets                              #
# ============================================================= #

sub poolstore_init {
    $builder = Gtk3::Builder->new();
    $builder->add_from_file("glade/poolstore.glade");
    $builder->connect_signals();
    $w_main = $builder->get_object('w_main');
    my $cs = Gtk3::CssProvider->new();
    $cs->load_from_file(Gio2::File::new_for_path("$ENV{HOME}/.config/poolstore/ps.css"));
    my $context = $w_main->get_style_context();
    #my $context = $builder->get_object("eb_maint")->get_style_context();
    #$context->add_provider($cs, 600);
    Gtk3::StyleContext::add_provider_for_screen($context->get_screen(),
        $cs, 600);
    # Connect here so customer_search_init can use it
    $dbh = DBI->connect("dbi:Pg:dbname=poolstore", '','') or
                die "Cannot connect to Server";
    # Set up connection string for StylePrint
    $sp_connstr = 'dbname=poolstore';
#    customer_search_init ();
    # Set up customer search
    $w_main->show_all;

    $newsalepkg = NewPoolSale->new;
    # Initialize the year-selection dialog

#    my $bldr = Gtk3::Builder->new();
#    $bldr->add_from_file("glade/pool_yr_sel.glade");
#    $bldr->connect_signals();
    my $yrs = $dbh->selectall_arrayref("SELECT min(date_part('year',saledate)),max(date_part('year',saledate)) FROM poolsales");

    unless ($yrs) {
        print ("Failed getting year range from Database: ${db->errstr}\n");
        die;
    }

    # Set up spinbutton
    my $y = $yrs->[0];    # We only have one row
    my $scroller = $builder->get_object('sel-yr-spinbtn');
    $scroller->set_range($y->[0],$y->[1]);
    $scroller->set_increments(1,5);
    $scroller->set_value($y->[1]);
    $yr_sel = $builder->get_object ('dlg_sel_yr');
}

# ============================================================= #
# Subroutine to allow user to select a year to parse.           #
# Called on entry to most functions.                            #
# ============================================================= #

sub select_yr {
    my $selected = 0;

    if ($yr_sel->run eq 'ok') {
        $selected = $cur_yr;
    }

    $yr_sel->hide;
    return $selected;
}

# ============================================================= #
# Function to retrieve data from database and place the data    #
# into a ListStore                                              #
# ============================================================= #

# ============================================================= #
# Function to rerieve data from database and place the          #
# data into a GtkGrid.                                          #
# Returns: The Grid on success,   undef on failure to           #
# retrieve data from the database or failure to create a        #
# grid                                                          #
# ============================================================= #

sub get_data_into_grid {
    my $aligns = pop;

    my $grid;
    my $data = $dbh->selectall_arrayref (@_);

    unless ($data) {
        print $dbh->errstr;
    }

    unless ($data) {
        return undef;
    }

    unless ($grid = Gtk3::Grid->new()) {
        return undef;
    }

    $grid->set_column_spacing(15);
    my $row;

    for ($row = 0; $row < (scalar(@$data)); $row++) {
        my $col = 0;
        my $rowdat = $data->[$row];

        for ($col = 0; $col < (scalar(@$rowdat)); $col++) {
            my $lbl = Gtk3::Label->new($rowdat->[$col]);
            $lbl->set_halign ($aligns->[$col]);
            $grid->attach($lbl ? $lbl : '', $col, $row, 1, 1);
        }
    }

    return $grid;
}

# ============================================================= #
# Convenience function to create a dialog window.               #
# This function is called by most fuctions.                     #
# ============================================================= #

sub create_dialog {
    my $dlg = Gtk3::Dialog->new ("Dialog",
        $w_main, 'destroy-with-parent', 'gtk-close'=>'ok'
    );

    $dlg->get_content_area->set_border_width(20);

    return $dlg;
}

# ============================================================= #
# a "full-feartured" routine to fetch data into a grid,         #
# create a dialog, populate the data into the grid, add         #
# the grid to the dialog, run and then destroy it.              #
# Passed: the data to pass to the database.. it can be          #
# either a simple query or a query with parameters.             #
# If there are parameters, be sure to pass a placeholder        #
# for the attribs between the query and the parameter list      #
# ============================================================= #

sub run_dialog_with_grid {
    my @vars = @_;
    my $grid;

    unless ($grid = get_data_into_grid(@vars)) { return 0; }

    my $dlg = create_dialog();
    unless ($dlg) { return 0; }
    $dlg->get_content_area->add ($grid);
    $dlg->show_all();
    $dlg->run;
    $dlg->destroy;
}


# ============================================================= #
#  Callback for when newsale button is clicked                  #
#  Brings up a dialog to add a new sale                         #
# ============================================================= #

sub on_newsale_clicked {
    my $btn = shift;
    $newsalepkg->run;
}

sub update_salestatus {
    my ($status,$id) = @_;

    my $qry = 'UPDATE poolsales SET status = ? WHERE sale_id=?';
    $dbh->do($qry, undef, $status, $id);
}

sub on_mi_item_gone_activate {
    my ($menuitm, $tv) = @_;
    my ($model,$itr) = $tv->get_selection->get_selected;
    update_salestatus('Gone', $model->get($itr, $model->get_n_columns - 1));
}

sub on_mi_item_forfeit_activate {
    my ($menuitm, $tv) = @_;
    my ($model,$itr) = $tv->get_selection->get_selected;
    update_salestatus('Forfeit', $model->get($itr, $model->get_n_columns - 1));
}

sub on_heldpool_treeview_button_release_event {
    my ($tv, $event) = @_;

    if ($event->button == 3) {
        $builder->get_object('markgone-popup')->popup (undef, undef,
            undef, undef, $event->button, $event->time);
    }
}

sub on_btn_gone_forfeit_clicked {
    my $qry = join(' ',
        qq/SELECT to_char(saledate,'MM-DD-YY') AS date,/,
        qq/fullname(firstname,mi,lastname) AS name,streetaddr,/,
        qq/concat_ws(' ',(concat_ws(', ',city,state),zip)) AS city,/,
        qq/get_poolsizename(poolid) AS pool,phone,cell,status,sale_id/,
        qq/FROM poolsalesfullview WHERE status='Hold' ORDER BY saledate/);

    my $rslt = $dbh->selectall_arrayref($qry);

    unless($rslt && length($rslt)) {
        return undef;
    }

    my $tv = $builder->get_object('heldpool-treeview');
    my $store = $tv->get_model;

    my $iter = $store->get_iter_first();

    if ($iter) {
        $store->clear;
    }

    foreach my $row (@$rslt) {
        $iter = $store->append;

        for (0 .. scalar(@$row) - 1) {
            $store->set($iter, $_, $row->[$_]);
        }
    }

    my $dlg = $builder->get_object('heldpool-dlg');
    $dlg->show_all;
    $dlg->run;
    $dlg->hide;
}

# ============================================================= #
# Displays sales of pools by size.                              #
# ============================================================= #

sub on_btn_pool_sales_by_size_clicked {
    unless (select_yr()) {
        return;
    }

    run_dialog_with_grid ( join (' ',
            "SELECT poolsize,count(poolsize) FROM poolsalesfullview",
            "WHERE date_part('year',saledate)=? AND status != 'Forfeit'",
            "GROUP BY poolsize ORDER BY count DESC"),
            undef, $cur_yr, ['end', 'end']);
}

# ============================================================= #
# Callback: Display poolsales for given year by type            #
# and size.                                                     #
# ============================================================= #

sub on_btn_poolsales_type_siz_clicked {
    unless (select_yr()) {
       return;
    }

    my $grid = get_data_into_grid("SELECT * FROM poolsales_by_type(?)",
        undef, $cur_yr, ['center', 'start', 'end']);

    if ($grid) {
        my $dlg = create_dialog();

        my $lbl = Gtk3::Label->new ();
        $lbl->set_markup ("<b>Poolsales by type ($cur_yr)</b>");
        $dlg->get_content_area->pack_start ($lbl, FALSE, FALSE, 5);

        # Add headers for grid
        $grid->insert_row (0);
        $lbl = Gtk3::Label->new;
        $lbl->set_markup ('<b>Size  </b>');
        $grid->attach ($lbl, 0, 0, 1, 1);
        $lbl = Gtk3::Label->new;
        $lbl->set_markup ('<b>Name</b>');
        $grid->attach ($lbl, 1, 0, 1, 1);
        $lbl = Gtk3::Label->new;
        $lbl->set_markup ('<b>Ttl</b>');
        $grid->attach ($lbl, 2, 0, 1, 1);
        $dlg->get_content_area->pack_start ($grid, FALSE, FALSE, 5);
        $dlg->get_content_area->pack_start (Gtk3::Separator->new('horizontal'),
            FALSE, FALSE, 5);
        $dlg->show_all();
        $dlg->run;
        $dlg->destroy;
    }
}

# ============================================================= #
# Returns count of pools sold in desired year broken down       #
# by size.                                                      #
# ============================================================= #

sub on_btn_heldpool_siz_clicked {
    unless (select_yr()) {
       return;
    }

    my $qry = "SELECT poolsize || ' ' || poolname AS pool,count(*) FROM poolsalesfullview WHERE date_part('year',saledate)=? AND status='Hold' GROUP BY pool";
    my @ttls = qw/Pool Ttl/;
    my $data = $dbh->selectall_arrayref ($qry, undef, $cur_yr);

    unless ($data and scalar(@$data)) {
        my $msg = 'No Data Returned...';

        if ($dbh->err) {
            $msg .= "\n" . $dbh->errstr;
            my $d = Gtk3::MessageDialog->new($w_main, 'destroy-with-parent',
                'error', 'gtk-close', $msg);
            $d->run;
            $d->destroy;
        }

        return;
    }

    my $dlg = Gtk3::Dialog->new("Held Pools by Size", $w_main,
        'destroy-with-parent', 'gtk-close' => 'ok');
    $dlg->set_default_geometry(200,400);

    my $store = Gtk3::ListStore->new("Glib::String","Glib::Int");
    my $tv = Gtk3::TreeView->new($store);

    for my $col (0 ... scalar(@ttls) - 1) {
        my $rend = Gtk3::CellRendererText->new;

        $rend->set(xalign=>1);

        my $attr = 'text';
        my $tvcol = Gtk3::TreeViewColumn->new_with_attributes (
            $ttls[$col], $rend, $attr=>$col);
        $tv->append_column($tvcol);
    }

    foreach my $row (@$data) {
        my @rowdat = ();

        for (0 ... ($tv->get_n_columns - 1)) {
            push @rowdat, $_;
            push @rowdat, $row->[$_];
        }

        $store->set ($store->append, @rowdat);
    }

    my $sw = Gtk3::ScrolledWindow->new();
    $sw->set_policy('automatic', 'automatic');
    $sw->set_vexpand('true');
    $tv->get_selection->set_mode('none');
    $sw->add($tv);
    $dlg->get_content_area->add($sw);
    $dlg->show_all();
    $dlg->run();
    $dlg->destroy();
}

# ============================================================= #
# Returns details on held pools including customer, date        #
# of sale, etc.                                                 #
# ============================================================= #

sub on_btn_heldpool_details_clicked {
    unless (select_yr()) {
       return;
    }

    my @ttls = qw(Date Customer Street City/State/Zip Pool);
    my $flds = qq/saledate,fullname(firstname,lastname) AS name,streetaddr,/ .
        qq/city || ', ' || state || ' ' || zip AS city,/ .
        "get_poolsizename(poolid)";


    my $qry = join ( ' ',
        "SELECT $flds FROM poolsalesfullview",
        "WHERE date_part('yr',saledate)=? AND status = 'Hold'",
        "ORDER BY saledate");
    my $data = $dbh->selectall_arrayref ($qry, undef, $cur_yr);

    unless ($data and scalar(@$data)) {
        my $msg = 'No Data Returned...';

        if ($dbh->err) {
            $msg .= "\n" . $dbh->errstr;
            my $d = Gtk3::MessageDialog->new($w_main, 'destroy-with-parent',
                'error', 'gtk-close', $msg);
            $d->run;
            $d->destroy;
        }

        return;
    }

    my $dlg = Gtk3::Dialog->new("Held Pool Details", $w_main,
        'destroy-with-parent', 'gtk-close' => 'ok');
    $dlg->set_default_geometry(700,500);

    my $store = Gtk3::ListStore->new("Glib::String","Glib::String",
        "Glib::String","Glib::String","Glib::String");
    my $tv = Gtk3::TreeView->new($store);

    for my $col (0 ... scalar(@ttls) - 1) {
        my $rend = Gtk3::CellRendererText->new;
        my $attr = 'text';
        my $tvcol = Gtk3::TreeViewColumn->new_with_attributes (
            $ttls[$col], $rend, $attr=>$col);
        $tv->append_column($tvcol);
    }

    foreach my $row (@$data) {
        my @rowdat = ();

        for (0 ... ($tv->get_n_columns - 1)) {
            push @rowdat, $_;
            push @rowdat, $row->[$_];
        }

        $store->set ($store->append, @rowdat);
    }

    my $sw = Gtk3::ScrolledWindow->new();
    $sw->set_policy('automatic', 'automatic');
    $sw->set_vexpand('true');
    $tv->get_selection->set_mode('none');
    $sw->add($tv);
    $dlg->get_content_area->add($sw);
    $dlg->show_all();
    $dlg->run();
    $dlg->destroy();
}

# ============================================================= #
# Callback for when the "Year-Sales" butto is clicked           #
# Shows a summary of the sales for a given year                 #
# ============================================================= #

sub on_btn_yr_sales_clicked {
    unless (select_yr()) {
       return;
    }

    my $qry = join (' ',
        "SELECT to_char(saledate,'MM-DD-YYYY') as date,",
        "fullname(firstname,mi,lastname),streetaddr,",
        "city || ', ' || state || ' ' || zip AS city,",
        "phone,cell,get_poolsizename(poolid) AS pool, status,sale_id",
        "FROM poolsalesfullview WHERE date_part('year',saledate)=?",
        "ORDER BY saledate");

    my $grid;

    unless ($grid = get_data_into_grid($qry,undef,$cur_yr, ['start', 'start',
                'start', 'start', 'start', 'start', 'start', 'end', 'end'])) {
         return 0;
    }

    my $dlg = create_dialog();
    $dlg->set_title("Pools Sales for Year $cur_yr");
    my $sw = Gtk3::ScrolledWindow->new();
    $sw->add($grid);
    $sw->set_policy('automatic','automatic');
    $sw->set_vexpand('true');
    $dlg->set_default_geometry (800,600);
    unless ($dlg) {return 0};
    $dlg->get_content_area->add ($sw);
    $dlg->show_all();
    $dlg->run();
    $dlg->destroy();

}

# ============================================================= #
# Displays yearly count of total pools sold for each year       #
# of record.                                                    #
# ============================================================= #

sub on_btn_sales_hist_clicked {
    run_dialog_with_grid("SELECT * FROM sales_hist", ['center', 'end']);
}

# ============================================================= #
# Displays monthly sales of pools for desired year.             #
# ============================================================= #

sub on_btn_mo_sum_sales_clicked {
    unless (select_yr()) {
       return;
    }

    run_dialog_with_grid ("SELECT mo,mocount,motot FROM msum(?)",
        undef, $cur_yr, ['start', 'end', 'end']);
}

# ============================================================= #
# Compares sales between two years.                             #
# ============================================================= #

sub on_btn_cmp_sales_clicked {
    # Get first year
    unless (select_yr()) {
       return;
    }

    my $yr1 = $cur_yr;

    # Get second year
    my $scroller = $builder->get_object('sel-yr-spinbtn');
    my ($first, $last) = $scroller->get_range;

    if ($yr1 > $first) {
        $scroller->set_value ($yr1 - 1);
    }

    unless (select_yr()) {
       return;
    }

    my $yr2 = $cur_yr;
    my $data = $dbh->selectall_arrayref("SELECT * FROM mosumcmp(?,?)",
        undef, $yr2, $yr1);
    my $tv;

    if ($data) {
        if (scalar @$data) {
            my $store = Gtk3::ListStore->new("Glib::String", "Glib::Int",
                "Glib::Int", "Glib::Int", "Glib::Int");
            $tv = Gtk3::TreeView->new($store);
            $tv->get_selection->set_mode('none');
            my @ttls = ('Mo');
            push @ttls, $yr2 . ' Sales';
            push @ttls, $yr2 . ' Tots';
            push @ttls, $yr1 . ' Sales';
            push @ttls, $yr1 . ' Tots';

            for my $col (0 ... scalar(@ttls) - 1) {
                my $rend = Gtk3::CellRendererText->new;

                if ($col) {
                    $rend->set (xalign=>1);
                }

                my $attr = 'text';

                my $tvcol = Gtk3::TreeViewColumn->new_with_attributes (
                    $ttls[$col], $rend, $attr=>$col);
                $tvcol->set_sort_column_id ($col);

                $tv->append_column ($tvcol);
            }

            foreach my $row (@$data) {
                my @rowdat = ();

                for (0 ... scalar(@$row) - 1) {
                    push @rowdat, $_;
                    push @rowdat, $row->[$_];
                }

                $store->set ($store->append, @rowdat);
            }
        }

        my $dlg = create_dialog();
        $dlg->set_title ("$yr1 Compared to $yr2");
        $dlg->get_content_area->add ($tv);
        $dlg->show_all();
        $dlg->run();
        $dlg->destroy();
    }
    else {
        printf($dbh->errstr);
    }
}

# ============================================================= #
# Callback to compare sales for this year with current date one #
# year ago.                                                     #
# ============================================================= #

sub on_btn_ytd_clicked {
    my $yrres = $dbh->selectrow_arrayref("SELECT date_part('year',now())");
    my $curyr = $yrres->[0];
    my $subq = "SELECT count(*) FROM one_year(?)";
    my $qry = qq/SELECT ($subq WHERE age(saledate) >= '1 year') AS "/ .
    ($curyr - 1) . qq/",($subq) AS "$curyr"/;
    my $rslt = $dbh->selectrow_arrayref($qry, undef, $curyr - 1, $curyr);
    my $grid = Gtk3::Grid->new;
    $grid->set_column_spacing(20);
    $grid->attach (Gtk3::Label->new(($curyr - 1) . ''), 0, 0, 1, 1);
    $grid->attach (Gtk3::Label->new($curyr), 1, 0, 1, 1);

    $grid->attach (Gtk3::Separator->new('horizontal'), 0, 1, 2, 1);
    $grid->attach (Gtk3::Label->new($rslt->[0] . ''), 0, 2, 1, 1);
    $grid->attach (Gtk3::Label->new($rslt->[1] . ''), 1, 2, 1, 1);
    my $dlg = create_dialog();
    $dlg->set_title($curyr . " vs " . ($curyr - 1) . "(YTD)");
    $dlg->get_content_area->add($grid);
    $dlg->show_all();
    $dlg->run;
    $dlg->destroy;
}

sub on_customer_view_button_ok_clicked {
}

# ============================================================= #
# Callback for when Customer Search Button on main window       #
# is clicked.                                                   #
# ============================================================= #

sub on_btn_cust_srch_clicked {
    unless ($customer_search) {
        $customer_search = CustomerSearch->new;
    }

    $customer_search->{dialog}->run();
    $customer_search->{dialog}->hide();
}

# ============================================================= #
# Callbacks for hardcopy printing                               #
# ============================================================= #

sub on_btn_prnt_addr_lbl_clicked {
}

sub on_sp_btn_cmp_another_clcked {
    # Get first year
    unless (select_yr()) {
       return;
    }

    my @yrs;
    push @yrs, $cur_yr;

    # Get second year
    my $scroller = $builder->get_object('sel-yr-spinbtn');
    my ($first, $last) = $scroller->get_range;

    if ($yrs[0] > $first) {
        $scroller->set_value ($yrs[0] - 1);
    }

    unless (select_yr()) {
       return;
    }

    push @yrs, $cur_yr;
    my $qry = 'SELECT * FROM mosumcmp($1,$2)';
    my $sp = StylePrint1::Pg->new or
        die ("Failed to initialize StyleprintTable");
    $sp->connect ($sp_connstr) or die ("StylePrint: Cannot to database");

    my $cmpxml = <<EOX
<config>
  <pageheader pointsbelow='9'>
    <cell percent='78' align='c' textsource='static' celltext="Monthly Pool Sales $yrs[0] vs $yrs[1]">
      <font weight='semibold' />
    </cell>
  </pageheader>
  <body>
    <header pointsbelow='5'>
      <cell percent='20' textsource='static' celltext='' />
      <cell percent='8' align='c' textsource='static' celltext='Mo' />
      <cell percent='6' align='c' textsource='static' celltext="$yrs[0]" />
      <cell percent='8' align='c' textsource='static' celltext="$yrs[0] Ttl" />
      <cell percent='7' align='c' textsource='static' celltext="$yrs[1]" />
      <cell percent='6' align='c' textsource='static' celltext="$yrs[1] Ttl"/>
    </header>
    <cell percent='20' textsource='static' celltext='' />
    <cell percent='8' align='r' textsource='data' celltext='mo' />
    <cell percent='7' align='r' textsource='data' celltext='m1count' />
    <cell percent='6' align='r' textsource='data' celltext='m1tot' />
    <cell percent='8' align='r' textsource='data' celltext='m2count' />
    <cell percent='6' align='r' textsource='data' celltext='m2tot' />
  </body>
</config>
EOX
;
    $sp->fromxmlstring ($w_main, $qry, \@yrs, $cmpxml);
}

sub build_main_window() {
    $w_main = Gtk3::Window->new('toplevel');
    $w_main->signal_connect('delete_event', \&kill_wmain);

    # The main container for all the widgets in the main window
    my $vbox_main = Gtk3::Box->new('vertical',3);
    my $main_menu = Gtk3::MenuBar->new;
    $vbox_main->add($main_menu);
    $w_main->show_all;
};

#build_main_window();
# We try touse builder...
poolstore_init();

Gtk3->main;

exit 0;

