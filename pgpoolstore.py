#! /usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkCalendar', '1.0')
gi.require_version('StylePrint', '1.0')
from gi.repository import Gtk, Gdk, Gio, GtkCalendar, StylePrint
import psycopg2 as mdb
import os

con = mdb.connect('dbname=poolstore');

w_main = None
builder = None
yr_sel = None
cur_yr = None
newsalepkg = None
sel_yr = {}
customer_search = {}
sp_connstr = 'dbname=poolstore'

class msg_dlg():

    def __init__(self, lvl, msg):
        Dlg = None
        levels = {"error": Gtk.MessageType.ERROR,
                "info": Gtk.MessageType.INFO,
                "warning": Gtk.MessageType.WARNING}
        
        dlg = Gtk.MessageDialog(w_main, 0, levels.get(lvl,
            Gtk.MessageType.INFO),
            Gtk.ButtonsType.CLOSE,
            msg)

    def run(self):
        Gtk.Dialog.run(self.Dlg)

    def __del__(self):
        Gtk.Object.destroy(self.Dlg)

class NewPoolSale:
    def __init__(self):
        self.bldr = Gtk.Builder()
        self.bldr.add_from_file('glade/newpoolsale.glade')
        self.dlg = self.bldr.get_object('new_pool_sale_dialog')
        self.dlg.set_transient_for(w_main)

        # Insert the GtkCalendarEntry into its place
        self.obj = {}
        self.customertable = {}
        self.poolsales = {}
        self.obj['date'] = GtkCalendar.Entry()
        self.bldr.get_object('grid-poolsale').attach(self.obj['date'],
                1, 0, 1, 1)
        self.populate_poolnames()
        self.populate_lastnames()

        # Set up hashes to correlate widgets to poolsale/customertable tables
        self.poolsales['saledate'] = self.obj['date']

        for cid in ('ns-status', 'ns-poolid', 'ns-p_comment'):
            widg = self.bldr.get_object(cid)
            pkey = cid
            self.poolsales[pkey.replace('ns-','')] = widg

        for cid in ('ns-firstname', 'ns-lastname', 'ns-mi', 'ns-streetaddr',
                'ns-city', 'ns-state', 'ns-zip', 'ns-phone', 'ns-cellphone'):
            wdg = self.bldr.get_object(cid)
            cid.replace('ns-','')
            self.customertable[cid.replace('ns-','')] = wdg

        # Connect some signals that couldn't be done outside the program
        self.customertable['zip'].connect('changed', self.on_ns_zip_changed)
        self.customertable['firstname'].connect('enter-notify-event',
                self.on_ns_firstname_enter_notify_event)
        self.bldr.get_object('ns-btn-save').connect('clicked',
                self.on_ns_btn_save_clicked)
        self.bldr.get_object('ns-btn-reset').connect('clicked',
                self.on_ns_btn_reset_clicked)

        # Connect the rest of the controls to a routine to check if all
        # necessary fields have been filled out, and enable/disable "Save" btn
        ct = self.customertable

        for ctl in (ct['firstname'], ct['lastname'], ct['streetaddr'],
                ct['city'], ct['state']):
            ctl.connect('changed', self.check_form)

    def on_ns_btn_reset_clicked(self, btn):
        for id in ['ns-p_comment', 'ns-firstname', 'ns-lastname', 'ns-mi',
        'ns-streetaddr', 'ns-city', 'ns-state', 'ns-zip',
        'ns-phone', 'ns-cellphone']:
            self.bldr.get_object(id).set_text('')

    def run(self):
        if self.bldr.get_object('ns-btn-save').get_sensitive():
            self.bldr.get_object('ns-btn-save').set_sensitive(False)

        self.dlg.run()
        self.dlg.hide()

    # ############################################################# #
    # Subroutine to check through all the widgets to to determine   #
    # all required fields have been filled out.  If so, the "Save"  #
    # button is enabled, if not, the button is set/left disabled    #
    # ############################################################# #

    def check_form(self, ctl):
        ct = self.customertable
        savebtn = self.bldr.get_object('ns-btn-save')

        for ctl in (ct['firstname'], ct['lastname'], ct['streetaddr'],
                ct['city'], ct['state'], ct['zip'],
                self.poolsales['saledate']):
            if not len(ctl.get_text()):

                if savebtn.get_sensitive():
                    savebtn.set_sensitive(False)

                break

        if not savebtn.get_sensitive():
            savebtn.set_sensitive(True);

    def populate_lastnames(self):
        qry = "SELECT DISTINCT lastname FROM customertable ORDER BY lastname"
        store = self.bldr.get_object('liststore-lastname')

        if store.get_iter_first():
            store.clear()

        cursor = con.cursor()
        cursor.execute(qry)
        names = cursor.fetchall()

        if len(names):
            for row in names:
                store.set(store.append(), 0, row[0])

    def populate_poolnames(self):
        qry = """SELECT cast(poolid AS char(5)),poolsize,poolname,
        concat(poolsize,' ',poolname)
        FROM poolnames
        ORDER BY pooltype DESC,disp_order"""

        curs = con.cursor()
        curs.execute(qry)
        rslt = curs.fetchall()
        curs.close()

        store = self.bldr.get_object('nps-poolnames-liststore')
        it = store.get_iter_first()

        if it:
            store.clear()

        for row in (rslt):
            itr = store.append()

            for col in range(0, len(row)):
                store.set(itr, col, row[col])

    def on_ns_zip_changed(self, ent):
        curzip = ent.get_text()
        self.cust_id = 0

        if len(curzip) == 5:
            qry = "SELECT city,state FROM zz_zipcodes WHERE zip=%s"
            curs = con.cursor()
            curs.execute(qry, (curzip,))
            cty = curs.fetchall()

            if len(cty):
                self.customertable['city'].set_text(cty[0][0])
                self.customertable['state'].set_text(cty[0][1])

            curs.execute(' '.join(("SELECT cust_id,streetaddr",
                    "FROM customertable WHERE firstname=%s and lastname=%s",
                    "and zip=%s")),
                    (self.customertable['firstname'].get_text(),
                        self.customertable['lastname'].get_text(),
                        curzip))
            custlist = curs.fetchall()
            curs.close()

            if len(custlist):
                resp = None
                addr = None

                for cust in custlist:
                    addr = cust[1]
                    dlg = Gtk.MessageDialog(self.dlg,
                            0,
                            Gtk.MessageType.QUESTION,
                            Gtk.ButtonsType.YES_NO,
                            "Is Street Address: '{0}' ?".format(addr))
                    resp = dlg.run()
                    dlg.destroy()

                    if resp == Gtk.ResponseType.YES:
                        self.customertable['streetaddr'].set_text(addr)
                        self.cust_id = cust[0]
                        break

        self.check_form(ent)

    def build_insert_query(self, tblnam, widgets, vals):
        cols = []
        placeholdr = []

        # We pass (colname,cust_id) in vals if we're doing the pool part
        if len(vals):
            cols.append(vals.pop(0))
            placeholdr.append('%s')

        for col,widg in widgets.items():
            if type(widg) == Gtk.RadioButton:
                grp = widg.get_group()

                for rb in grp:
                    if rb.get_active():
                        value = rb.get_label()
                        break
            else:
                if type(widg) == Gtk.ComboBox:
                    value = widg.get_active_id()
                else:
                    value = widg.get_text()

            if value:
                cols.append(col)
                vals.append(value)
                placeholdr.append('%s')

        return "INSERT INTO {0} ({1}) VALUES({2})".format(
                tblnam, ",".join(cols), ",".join(placeholdr))

    # ============================================================= #
    # Callback for when the New Pool Sale "Save" button is clicked  #
    # Retrieves the data from the controls and inerts it into the   #
    # database.                                                     #
    # ============================================================= #

    def on_ns_btn_save_clicked(self, btn):
        p_vals = []
        c_vals = []

        if not self.cust_id:
            custqry = self.build_insert_query ('customertable',
                    self.customertable, c_vals)
            cursor = con.cursor()

            try:
                cursor.execute(custqry, c_vals)
                self.cust_id = cursor.lastrowid
            except psycopg2.Error as e:
                d = msg_dlg("error", "Error: %s" % e.pgerror)
                #d = Gtk.MessageDialog(w_main, 0, Gtk.MessageType.ERROR,
                #        Gtk.ButtonsType.CLOSE,
                #        ("Error: %s" % e.pgerror))
                d.run()
                #d.destroy()
                del msg_dlg
                #print ("Error: %s" % e.pgerror)

        p_vals = ['cust_id', self.cust_id]
        poolqry = self.build_insert_query('poolsales', self.poolsales, p_vals)
        cursor = con.cursor()

        try:
            cursor.execute(poolqry, p_vals)
        except psycopg2.Error as e:
            d = Gtk.MessageDialog(w_main, 0, Gtk.MessageType.ERROR,
                    Gtk.ButtonsType.CLOSE,
                    ("Error: %s" % e.pgerror))
            d.run()
            d.destroy()
        con.commit()

    def on_ns_firstname_enter_notify_event(self, ent, event):
        ln = self.bldr.get_object('ns-lastname').get_text()

        if len(ln):
            qry = """SELECT DISTINCT firstname from customertable
            WHERE lower(lastname) = lower(%s) ORDER BY firstname"""
            cursor = con.cursor()
            cursor.execute(qry,(ln,))
            firsts = cursor.fetchall()

            if len(firsts):
                store = self.bldr.get_object('liststore-firstname')

                if store.get_iter_first():
                    store.clear()

                for fn in firsts:
                    store.set(store.append(), 0, fn[0])

class CustomerSearch:
    def __init__(self, wmain):
        global con

        hndlrs = {
            "on_cs_firstname_enter_notify_event": self.firstname_enter_event,
            "on_cust_view_multi_button_ok_clicked": self.custview_multi_btn_ok
        }

        self.wmain = wmain
        self.builder = Gtk.Builder()
        self.builder.add_from_file ('glade/poolcustomersearch.glade')
        self.builder.connect_signals (hndlrs)
        self.dialog = self.builder.get_object('dlg_cust_srch')
        self.dialog.set_transient_for (self.wmain)
        self.builder.get_object(
                'dlg_customer_view_multi').set_transient_for(wmain)
        self.buttons = {}
        self.buttons['phone'] = self.builder.get_object('rb-cust-phone')
        self.buttons['phone'].connect ('clicked', self.rb_cust_phone_clicked)
        self.buttons['name'] = self.builder.get_object ('rb-cust-name')
        self.buttons['name'].connect ('clicked', self.rb_cust_name_clicked)
        self.names = {}
        self.names['last'] = self.builder.get_object('cs_lastname')
        self.names['first'] = self.builder.get_object('cs_firstname')
        self.names['mi'] = self.builder.get_object('cs_mi')
        self.phone = self.builder.get_object('cs-phone-num')
        self.builder.get_object ('cs-btn-srch').connect ('clicked',
                self.cs_cust_srch)

        # set up lastname features
        self.liststore_lastname = self.builder.get_object (
                'liststore_cs_lastname')
        comp = self.names['last'].get_completion()
        comp.set_text_column(0)

        # now firstname
        self.liststore_firstname = self.builder.get_object(
                'liststore_cs_firstname')
        comp = self.names['first'].get_completion()
        comp.set_text_column(0)
        flds = 'lastname'

        for t in [self.names['last'], self.names['first']] :
            l = t.get_completion().get_model()
            q = """SELECT DISTINCT {0} AS name FROM customertable
            ORDER BY name""".format (flds)
            cur = con.cursor()
            cur.execute(q)
            y = cur.fetchall()
            cur.close()

            for row in y :
                l.set(l.append(), 0, row[0])

            flds = 'firstname'

    # ============================================================= #
    # Callback for when customer search "Search" button is clicked. #
    # Checks whether searching by phone # or name, then performs    #
    # database search for customer.                                 #
    # ============================================================= #

    def cs_cust_srch(self, btn):
        qry = "SELECT cust_id FROM customertable WHERE "
        binds = []
        flds = []

        if self.phone.get_sensitive() :
            srchnum = self.phone.get_text()

            if len(srchnum) < 4 :
                d = Gtk.MessageDialog(w_main, 0, Gtk.MessageType.INFO,
                        Gtk.ButtonsType.CLOSE,
                        "Please enter at least 4 of the last digits of number")
                d.run()
                d.destroy()

            qry += "phone LIKE %s or cell LIKE %s"
            binds = ('%' + srchnum, '%' + srchnum)
        else :
            lastname = self.names['last'].get_text()
            firstname = self.names['first'].get_text()

            if lastname :
                if len(lastname) > 0 :
                    binds.append(lastname)
                    flds.append(' lastname LIKE %s')

            if len(firstname):
                binds.append(firstname)
                flds.append ('firstname LIKE %s')

            qry += ' AND '.join(flds)

        crs = con.cursor()
        crs.execute(qry, binds)
        reslt = crs.fetchall()
        crs.close()

        if len(reslt) == 0 :
            md = Gtk.MessageDialog(w_main, 0, Gtk.MessageType.WARNING,
                    Gtk.ButtonsType.CLOSE,
                    """No customers with provided citera were found
                    Returning to try again...""")
            md.run()
            md.destroy()
            return
        elif len(reslt) == 1 :
            self.display_customer_with_pools (reslt[0])
        else :
            id = self.select_cust_from_ids(reslt)
#            if id:
#                self.display_customer_with_pools(id)

    # ----------------------------------------------------------------- #
    # Callback for when the OK button of the multi-customer display is  #
    # clicked.  It provides the id for the selected customer and then   #
    # calls the display_customer_with_pools() function                  #
    # ----------------------------------------------------------------- #

    # NOTE: the parameter list jumbled as per Python
    def custview_multi_btn_ok(self, treeview):
        model, treeiter = treeview.get_selection().get_selected()
        self.display_customer_with_pools(model[treeiter][7])

    def rb_cust_phone_enable(self):
        self.phone.set_sensitive(True)

        for k in self.names.keys():
            self.names[k].set_sensitive(False)

    def rb_cust_name_enable(self):
        self.phone.set_sensitive(False)

        for k in self.names.keys():
            self.names[k].set_sensitive(True)

    def rb_cust_phone_clicked(self,rb):
        if rb.get_active:
            self.rb_cust_phone_enable()
        else:
            self.rb_cust_name_enable()

    def rb_cust_name_clicked(self, rb):
        if rb.get_active:
            self.rb_cust_name_enable()
        else:
            self.rb_cust_phone_enable()

    # ================================================================= #
    # Callback for when the customer search firstname entry acquires    #
    # focus.  It refills the completion store with the firstnames that  #
    # match the lastname, if it is available.                           #
    # ================================================================= #

    def firstname_enter_event (self, entry, direction):
        # NOTE: The Python interface has things messed up.
        # The parameters should be cs_firstname,direction,cs_lastname
        # but the seem to be swapped, with cs_lastname missing
        # Therefore we'll just use builder to get the objects
        lastname = self.builder.get_object('cs_lastname').get_text()

        if lastname:
            qry = """SELECT DISTINCT firstname FROM customertable
            WHERE lastname LIKE %s ORDER BY firstname"""
            cursor = con.cursor()
            cursor.execute(qry, (lastname,))
            rslt = cursor.fetchall()
            cursor.close()

            if len(rslt):
                # NOTE: should have been entry
                lstore = self.builder.get_object('liststore_cs_firstname')
                lstore.clear()

                for na in rslt:
                    if na:
                        lstore.set(lstore.append(), 0, na[0])
                    else:
                        print("\nUndefined value in result")
                        return

    def select_cust_from_ids(self, id):
        placeholders = []
        binds = []
        dlg = self.builder.get_object('dlg_customer_view_multi')
        view = self.builder.get_object('treeview_customer_data')

        for i in id:
            placeholders.append('cust_id=%s')
            binds.append(i)

                #"city,state,zip,CAST(cust_id AS char) AS id",
        q1 = ("SELECT lastname,firstname,coalesce(mi,''),streetaddr,",
                "city,state,zip,CAST(cust_id AS text) AS id",
                "FROM poolsalesfullview WHERE ")
        qry = ' '.join(q1) + ' OR '.join(placeholders)
        cursor = con.cursor()
        cursor.execute(qry, binds)
        custs = cursor.fetchall()

        if len(custs) < 1:
            msg = "No data was retrieved..."
            # Print message in dialog later.
            print (msg)

        view.get_model().clear()

        for row in  custs:
            cols = []
            vals = []
            iter = view.get_model().append()

            if not view.get_model().iter_is_valid(iter):
                d = Gtk.MessageDialog(dlg,0, Gtk.DialogType.WARNING,
                        Gtk.ButtonsType.OK,"Invalid iter for TreeView")
                return

            for x in range(0, len(row)):
                view.get_model().set(iter, x, row[x])

        dlg.run()
        dlg.hide()

    # ================================================================= #
    # Function to display a customer (identified by id) and all the     #
    # pools this customer has purchased                                 #
    # ================================================================= #

    def display_customer_with_pools (self, custid) :
        global w_main

        qry = """SELECT fullname(firstname,mi,lastname),
        streetaddr,city,state,zip,cust_id FROM customertable
        WHERE cust_id=%s"""
        curs = con.cursor()
        curs.execute(qry,(custid,))
        cust = curs.fetchone()

        if len(cust) :
            poolqry = """SELECT concat(poolsize, ' ', poolname),saledate
            FROM (SELECT * FROM poolsales WHERE cust_id=%s) AS a
            LEFT JOIN poolnames USING (poolid)"""

        crs = con.cursor()
        crs.execute(poolqry, (custid,))
        pools = crs.fetchall()
        crs.close()

        if len(pools) == 0:
            return

        dialog = Gtk.Dialog()
        dialog.set_title("Customer Info")
        dialog.set_transient_for(w_main)
        dialog.add_button("Close", Gtk.ResponseType.OK)
        dialog.get_content_area().set_spacing(10)
        dialog.get_content_area().set_border_width(10)
        frame = Gtk.Frame()
        frame.set_label("Customer")
        frame.set_label_align(0.10, 0.50)
        grid = Gtk.Grid()
        grid.set_row_spacing (5)
        grid.set_border_width(5)
        grid.set_column_spacing(20)
        lbls = ('Name:','Address:', 'City:', 'State:', 'Zip:')

        for x in range(0, len(lbls)):
            lbl = Gtk.Label.new(lbls[x])
            lbl.set_xalign(1.0)
            grid.attach(lbl, 0, x + 1, 1,1);
            lbl = Gtk.Label.new(cust[x])
            lbl.set_xalign(0.0)
            grid.attach(lbl,1, x + 1, 1, 1)

        x += 2
        frame.add(grid)
        dialog.get_content_area().add(frame)

        # Print pool(s) sold to customer
        frame = Gtk.Frame()
        frame.set_label("Pools")
        frame.set_label_align(0.10, 0.50)
        grid = Gtk.Grid()
        grid.set_row_spacing (5)
        grid.set_border_width(5)
        grid.set_column_spacing(20)
        x += 1

        for p in pools:
            lbl = Gtk.Label.new(p[0])
            lbl.set_xalign(0.0)
            grid.attach(lbl, 1, x, 1, 1)
            grid.attach(Gtk.Label.new(' - '), 2, x, 1, 1)
            grid.attach(Gtk.Label.new(p[1].strftime("%m-%d-%Y")), 3, x, 1, 1)
            x += 1

        frame.add(grid)
        dialog.get_content_area().add(frame)
        dialog.show_all()
        dialog.run()
        dialog.destroy()

def select_yr():
    selected = 0

    if yr_sel.run() == Gtk.ResponseType.OK:
        selected = cur_yr

    yr_sel.hide()
    return selected


def not_implemented():
    md = Gtk.MessageDialog(parent=w_main, type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            message_format="This function has not yet been implemented")
    md.run();
    md.destroy();

def do_newsale(btn):
    newsalepkg.run()

# ============================================================= #
# Function to retrieve data from database and place the data    #
# int a GtkGrid.                                                #
# Returns the new GtkGrid on success, None on failure to        #
# retrieve data from the database, or failure to create a grid  #
# ============================================================= #

def get_data_into_grid(query, data, aligns):

    grid = None
    cursor = con.cursor()
    cursor.execute (query, data)
    rslt = cursor.fetchall()

    if not (rslt and (len(rslt))):
        md = Gtk.MessageDialog(w_main, 0, Gtk.MessageType.WARNING,
                Gtk.ButtonsType.CLOSE, "No Data was returned")
        md.run()
        md.destroy()
        return None

    grid = Gtk.Grid()

    if not grid:
        return None

    grid.set_column_spacing(10)
    row = 0

    for rowdat in rslt:
        for col in range(0, len(rowdat)):
            lbl = Gtk.Label.new("{0}".format(rowdat[col]))
            lbl.set_halign (aligns[col])
            grid.attach(lbl or '', col, row, 1, 1)

        row += 1

    return grid

# ============================================================= #
# Convenience function to create a dialog window.               #
# This function is called by many functions.                    #
# ============================================================= #

def create_dialog():
    dlg = Gtk.Dialog("Dialog", w_main, 0)
    dlg.add_button("Close", Gtk.ResponseType.OK)
    dlg.get_content_area().set_border_width(20)
    dlg.get_content_area().pack_end(
            Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL), 0, 0, 5)
    return dlg

# ============================================================= #
# A "full-featured" routine to fetch data into a grid, create   #
# a dialog, populate the data into the grid, add the grid to    #
# the dialog, run and destroy the dialog.                       #
# Passed: The query to pass to the database.. It can be either  #
# a simple query or a query with parameters.                    #
# ============================================================= #

def run_dialog_with_grid(qry, params, aligns):
    grid = get_data_into_grid(qry,params, aligns)

    if not grid:
        return 0

    dlg = create_dialog()

    if not dlg:
        return 0

    dlg.get_content_area().pack_start(grid, 0, 0, 10)
    dlg.show_all()
    dlg.run()
    dlg.destroy()

def update_salestatus(status,id):
    qry = 'UPDATE poolsales SET status=%s WHERE sale_id=%s'
    cursor = con.cursor()

    try:
        cursor.execute(qry,(status,id))
        con.commit()
    except:
        con.rollback()

# PyGobject mangles the parameters, tv should be second parameter
def mark_gone(tv):
    model,it = tv.get_selection().get_selected()
    update_salestatus('Gone',model[it][-1])

def mark_forfeit(tv):
    model,it = tv.get_selection().get_selected()
    update_salestatus('Forfeit',model[it][-1])

def heldpool_button_release(tv, event):
    if event.button == 3:
        builder.get_object('markgone-popup').popup(None,None,None,None,
                event.button,event.time)

# ============================================================= #
# Callback for when mark gone/forfeit is clicked                #
# ============================================================= #

def mark_gone_forfeit(btn):
    qry = """SELECT to_char(saledate, 'MM-DD-YYYY') as date,
        fullname(firstname,mi,lastname) AS name,streetaddr,
        concat(city, ', ', state, ' ', zip) AS city,
        get_poolsizename(poolid) AS pool,phone,cell,status,sale_id
        FROM poolsalesfullview WHERE status='Hold' ORDER BY saledate"""

    cursor = con.cursor()
    cursor.execute(qry)
    rslt = cursor.fetchall()

    if len(rslt) == 0:
        return

    tv = builder.get_object('heldpool-treeview')
    store = tv.get_model();

    if len(store):
        store.clear()

    for row in rslt:
        itr = store.append(row)

    dlg = builder.get_object('heldpool-dlg')
    dlg.show_all()
    dlg.run()
    dlg.hide()

# ============================================================= #
# ============================================================= #

def poolsales_by_size(btn):
    if not select_yr():
        return

    run_dialog_with_grid("""SELECT poolsize,count(poolsize) AS count
    FROM poolsalesfullview
    WHERE extract(year FROM saledate)=%s AND status !='Forfeit'
    GROUP BY poolsize ORDER BY count desc""", (cur_yr,),
    (Gtk.Align.START, Gtk.Align.END))

# ============================================================= #
# Callback: Display poolsales for given year by type and size   #
# ============================================================= #

def poolsales_by_typesize(btn):
    if not select_yr():
        return

    run_dialog_with_grid("  select * from poolsales_by_type(%s)", (cur_yr,),
            (Gtk.Align.START, Gtk.Align.START, Gtk.Align.END))

# ============================================================= #
# Displays the count of held pools pools sold in specified year #
# broken dow by size                                            #
# ============================================================= #

def heldpool_siz(btn):
    if not select_yr():
        return

    qry = """SELECT concat(poolsize,' ',poolname) AS pool, count(*) AS count
    FROM poolsalesfullview WHERE extract(year FROM saledate)=%s AND status='Hold'
    GROUP by pool"""
    ttls = ('Pool', 'Ttl')
    cursor = con.cursor()
    cursor.execute (qry, (cur_yr,))
    rslt = cursor.fetchall()

    if not (rslt and (len(rslt))):
        md = Gtk.MessageDialog(w_main, 0, Gtk.MessageType.WARNING,
                Gtk.ButtonsType.CLOSE, "No Data was returned")
        md.run()
        md.destroy()
        return None

    dlg = Gtk.Dialog()
    dlg.set_title("Held Pools by Size")
    dlg.set_transient_for(w_main)
    dlg.add_button("Close", Gtk.ResponseType.OK)
    dlg.set_default_size(200, 400)

    store = Gtk.ListStore(str, int)
    tv = Gtk.TreeView.new()
    tv.set_model(store)

    for col in range(0, len(ttls)):
        rend = Gtk.CellRendererText()
        rend.set_alignment(1.0, 0.50)
        tv.append_column(Gtk.TreeViewColumn(ttls[col], rend, text=col))

    for row in rslt:
        store.append(row)

    sw = Gtk.ScrolledWindow()
    sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
    sw.set_vexpand(True)
    tv.get_selection().set_mode(Gtk.SelectionMode.NONE)
    sw.add(tv)

    dlg.get_content_area().add(sw)
    dlg.show_all()
    dlg.run()
    dlg.destroy()

# ============================================================= #
# Displays details on held pools including customer,            #
# date of sale, poolsize, etc                                   #
# ============================================================= #

def heldpool_details(btn):
    if not select_yr():
        return

    ttls = ['Date', 'Customer', 'Street', 'City/State/Zip', 'Pool']
    flds ="""to_char(saledate, 'MM-DD-YYYY'),
    fullname(firstname,mi,lastname) AS name, streetaddr, concat(city,',',
    state,' ',zip) AS city, concat(poolsize, ' ',poolname)"""

    qry = """SELECT {0} FROM poolsalesfullview
    WHERE extract(year FROM saledate)=%s AND status='Hold'
    ORDER BY saledate""".format(flds)

    cursor = con.cursor()
    cursor.execute(qry, (cur_yr,))
    data = cursor.fetchall()
    cursor.close()

    if not (data and len(data)):
        md = Gtk.MessageDialog(w_main, 0, Gtk.MessageType.INFO,
                Gtk.ButtonsType.CLOSE,
                'No Held Pools for {0}'.format(cur_yr))
        md.run()
        md.destroy()
        return

    dlg = Gtk.Dialog()
    dlg.set_title("Held Pools by Details")
    dlg.set_transient_for(w_main)
    dlg.add_button("Close", Gtk.ResponseType.OK)
    dlg.set_default_size(700, 200)
    store = Gtk.ListStore(str, str, str, str, str)
    tv = Gtk.TreeView()
    tv.set_model(store)

    for col in range(0, len(ttls)):
        tv.append_column(Gtk.TreeViewColumn(ttls[col],
            Gtk.CellRendererText(), text=col))

    for row in data:
        store.append(row)

    sw = Gtk.ScrolledWindow()
    sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
    sw.set_vexpand(True)
    tv.get_selection().set_mode(Gtk.SelectionMode.NONE)
    sw.add(tv)

    dlg.get_content_area().add(sw)
    dlg.show_all()
    dlg.run()
    dlg.destroy()

# ============================================================= #
# Callback for when the "Year=Sales" button is clicked          #
# Shows a summary of the sales for a given year.                #
# ============================================================= #

def yr_sales(btn):
    if not select_yr():
        return

    qry = ("""SELECT to_char(saledate, 'MM-DD-YYYY') AS date,
            fullname(firstname,mi,lastname),streetaddr,
            concat(city,', ',state,' ',zip) AS city,
            phone,cell,concat(poolsize,' ',poolname) AS pool,status,sale_id
            FROM poolsalesfullview where extract(year FROM saledate)=%s
            ORDER BY saledate""")

    grid = get_data_into_grid(qry, (cur_yr,), (Gtk.Align.START,
            Gtk.Align.START, Gtk.Align.START, Gtk.Align.START,
            Gtk.Align.START, Gtk.Align.START, Gtk.Align.START,
            Gtk.Align.END, Gtk.Align.END))

    if not grid:
        return 0

    if len(grid) == 0:
        return 0

    dlg = create_dialog()
    dlg.set_title("Pools sold for year {0}".format(cur_yr))
    sw = Gtk.ScrolledWindow()
    sw.add(grid)
    sw.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
    sw.set_vexpand('true')
    dlg.set_default_size(800,600)

    if not dlg:
        return 0

    dlg.get_content_area().add(sw)
    dlg.show_all()
    dlg.run()
    dlg.destroy()

# ============================================================= #
# Display yearly count of total pools sold for each year        #
# of record                                                     #
# ============================================================= #

def sales_hist(btn):
    run_dialog_with_grid("SELECT * FROM sales_hist", None,
    (Gtk.Align.CENTER, Gtk.Align.END))

# ============================================================= #
# Displays monthly sales of pools for selected year.            #
# ============================================================= #

def mosum_sales(btn):
    if not select_yr():
        return

    run_dialog_with_grid("SELECT * FROM mosum(%s)", (cur_yr,),
            (Gtk.Align.START, Gtk.Align.START, Gtk.Align.END))

# ============================================================= #
# Compare sales between two years.                              #
# ============================================================= #

def cmp_sales(btn):
    # Get first year
    if not select_yr():
        return

    yr1 = cur_yr

    # Get second year
    scroller = builder.get_object('sel-yr-spinbtn')
    (first, last) = scroller.get_range()

    if yr1 > first:
        scroller.set_value(yr1 - 1)

    if not select_yr():
        return

    yr2 = cur_yr
    cursor = con.cursor()
    cursor.execute("SELECT * FROM mosumcmp(%s,%s)", (yr2,yr1))
    data = cursor.fetchall()

    if not (data and len(data)):
        md = Gtk.MessageDialog(w_main, 0, GtkMessageType.INFO,
                GtkButtonstype.CLOSE,
                "No data retrieved")
        md.run()
        md.destroy()
        return

    store = Gtk.ListStore(str, int, int, int, int)
    tv = Gtk.TreeView()
    tv.set_model(store)
    ttls = ['Mo',]
    ttls.append('{0} Sales'.format(yr2))
    ttls.append('{0} Tots'.format(yr2))
    ttls.append('{0} Sales'.format(yr1))
    ttls.append('{0} Tots'.format(yr1))

    for col in range(0, len(ttls)):
        rend = Gtk.CellRendererText()

        if col:
            rend.set_alignment(1.0, 0.5)

        tvcol = Gtk.TreeViewColumn(ttls[col], rend, text=col)
        tvcol.set_sort_column_id(col)
        tv.append_column(tvcol)

    for c in data:
        store.append(c)

    dlg = create_dialog()
    dlg.set_title('{0} Compared to {1}'.format(yr1, yr2))
    dlg.get_content_area().add(tv)
    dlg.show_all()
    dlg.run()
    dlg.destroy()

# ============================================================= #
# Callback to print Y-T-D com,parison between current year      #
#          and previous year                                    #
# ============================================================= #

def btn_ytd_screen(btn):
    cursor = con.cursor()
    cursor.execute("SELECT extract(year FROM now())")
    yrres = cursor.fetchone()
    curyr = yrres[0]
    cursor.close()
    subq = 'SELECT count(*) FROM poolsales WHERE extract(year FROM saledate)=%s'
    qry = """SELECT * FROM ({0} AND saledate <= now() - interval '1 year') AS a,
    ({0}) AS b""".format(subq)
    cursor = con.cursor()
    cursor.execute(qry, (curyr-1,curyr))
    rslt = cursor.fetchone()
    cursor.close()
    dlg = create_dialog()
    dlg.set_title("{0} vs {1}".format(curyr - 1, curyr))
    grid = Gtk.Grid()
    grid.set_column_spacing(20)
    grid.attach(Gtk.Label.new("{0}".format(curyr - 1)), 0, 0, 1, 1)
    grid.attach(Gtk.Label.new("{0}".format(curyr)), 1, 0, 1, 1)
    grid.attach(Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL),
            0, 1, 2, 1)
    grid.attach(Gtk.Label.new("{0}".format(rslt[0])), 0, 2, 1, 1)
    grid.attach(Gtk.Label.new("{0}".format(rslt[1])), 1, 2, 1, 1)
    dlg.get_content_area().add(grid)
    dlg.get_content_area().show_all()
    dlg.run()
    dlg.destroy()

# ============================================================= #
# Callback for when Customer Search Button on main window       #
# is clicked                                                    #
# ============================================================= #

def cust_srch(btn):
    global customer_search

    if not customer_search:
        customer_search = CustomerSearch(w_main)

    customer_search.dialog.run()
    customer_search.dialog.hide()

# ========================================================= #
# Callbacks for hardcopy printing                           #
# ========================================================= #

def prt_addr_lbls():
    not_implemented()

def sp_cmp_another(btn):
    global w_main, builder, yr_sel
    yrs = []

    if not select_yr():
        return
    yrs.append (str(cur_yr))

    # Get second year
    scroller = builder.get_object('sel-yr-spinbtn')
    (first, last) = scroller.get_range()

    if int(yrs[0]) > first:
        scroller.set_value(int(yrs[0]) - 1)

    if not select_yr():
        return
    yrs.append (str(cur_yr))

    qry = 'SELECT * FROM mosumcmp($1,$2)'
    sp = StylePrint.Pg()
    sp.connect(sp_connstr)

    cmpxml = """<config>
  <pageheader pointsbelow='9'>
    <cell percent='78' align='c' textsource='static' celltext='Monthly Pool Sales {0} vs {1}'>
      <font weight='semibold' />
    </cell>
  </pageheader>
  <body>
    <header pointsbelow='5'>
      <cell percent='20' textsource='static' celltext='' />
      <cell percent='8' align='c' textsource='static' celltext='Mo' />
      <cell percent='6' align='c' textsource='static' celltext='{0}' />
      <cell percent='8' align='c' textsource='static' celltext='{0} Ttl' />
      <cell percent='7' align='c' textsource='static' celltext='{1}' />
      <cell percent='6' align='c' textsource='static' celltext='{1} Ttl'/>
    </header>
    <cell percent='20' textsource='static' celltext='' />
    <cell percent='8' align='r' textsource='data' celltext='mo' />
    <cell percent='7' align='r' textsource='data' celltext='m1count' />
    <cell percent='6' align='r' textsource='data' celltext='m1tot' />
    <cell percent='8' align='r' textsource='data' celltext='m2count' />
    <cell percent='6' align='r' textsource='data' celltext='m2tot' />
  </body>
</config>"""
    sp.fromxmlstring(w_main, qry, yrs, cmpxml.format(yrs[0],yrs[1]))

# Another Python paramter shuffle
def yr_ret(yr):
    global cur_yr

    cur_yr = yr.get_value_as_int()

def poolstore_init():
    global w_main, builder, yr_sel, newsalepkg

    builder = Gtk.Builder()
    builder.add_from_file("glade/poolstore.glade")
    w_main = builder.get_object("w_main")

    # Load Css
    cs = Gtk.CssProvider.new()
    cs.load_from_file(Gio.File.new_for_path(os.environ['HOME'] + "/.config/poolstore/ps.css"))
    ctx = w_main.get_style_context()
    Gtk.StyleContext.add_provider_for_screen(ctx.get_screen(), cs, 600)

    w_main.show_all()

    handlers = {
        "kill_wmain": Gtk.main_quit,
        "on_newsale_clicked": do_newsale,
        "on_btn_gone_forfeit_clicked": mark_gone_forfeit,
        "on_btn_pool_sales_by_size_clicked": poolsales_by_size,
        "on_btn_poolsales_type_siz_clicked": poolsales_by_typesize,
        "on_btn_heldpool_siz_clicked": heldpool_siz,
        "on_btn_heldpool_details_clicked": heldpool_details,
        "on_btn_yr_sales_clicked": yr_sales,
        "on_btn_sales_hist_clicked": sales_hist,
        "on_btn_mo_sum_sales_clicked": mosum_sales,
        "on_btn_cmp_sales_clicked": cmp_sales,
        "on_btn_ytd_clicked": btn_ytd_screen,
        "on_btn_cust_srch_clicked": cust_srch,
        "on_btn_prnt_addr_lbl_clicked": prt_addr_lbls,
        "on_mi_item_gone_activate": mark_gone,
        "on_mi_item_forfeit_activate": mark_forfeit,
        "on_heldpool_treeview_button_release_event": heldpool_button_release,
        "sel_yr_ret": yr_ret,
        "on_newsale_clicked": do_newsale,
        "on_sp_btn_cmp_another_clcked": sp_cmp_another
    }

    newsalepkg = NewPoolSale()
    builder.connect_signals(handlers)

    cur = con.cursor()
    cur.execute("SELECT min(y),max(y) FROM (select distinct extract(year FROM saledate) y FROM poolsales) s")
    y = cur.fetchone()
    cur.close()
    scroller = builder.get_object('sel-yr-spinbtn')

    if (not y[0]):
        cur = con.cursor()
        cur.execute("SELECT year(now()) - 1, year(now())")
        y = cur.fetchone()
        cur.close()

    scroller.set_range(y[0],y[1])
    scroller.set_increments(1, 5)
    scroller.set_value(y[1])
    yr_sel = builder.get_object('dlg_sel_yr')

poolstore_init()
#customer_search_init()
Gtk.main()
