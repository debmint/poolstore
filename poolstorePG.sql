--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Debian 11.5-1+deb10u1)
-- Dumped by pg_dump version 11.5 (Debian 11.5-1+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: mosumtype; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.mosumtype AS (
	mo character varying(5),
	mosales integer,
	motot integer
);


--
-- Name: poolsalesbytype; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.poolsalesbytype AS (
	poolsize text,
	poolname text,
	totsales integer
);


--
-- Name: fullname(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.fullname(text, text) RETURNS text
    LANGUAGE sql
    AS $_$

    SELECT fullname($1, NULL, $2);
$_$;


--
-- Name: fullname(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.fullname(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$

DECLARE fn text;
DECLARE tmpnam text;
DECLARE count integer;
BEGIN
  fn := '';

  IF $1 IS NOT NULL THEN
    fn := $1;
  END IF;

  count := 2;
  tmpnam := $2;

  FOR count in 2..3

  LOOP
    IF tmpnam IS NOT NULL THEN
      IF length(fn) > 0 THEN
        fn := fn || ' ' || tmpnam;
      ELSE
        fn := tmpnam;
      END IF;
    END IF;

    tmpnam := $3;
  END LOOP;

  RETURN fn;
END;
$_$;


--
-- Name: get_poolid(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_poolid(poolsize text, poolname text) RETURNS integer
    LANGUAGE sql
    AS $_$
    SELECT poolid FROM poolnames WHERE poolsize=$1 AND poolname=$2;
$_$;


--
-- Name: FUNCTION get_poolid(poolsize text, poolname text); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION public.get_poolid(poolsize text, poolname text) IS '
Returns the poolID for the pool with the given poolsize and poolname.
Passed: $1 = poolsize (text), $2 = poolname (text).
';


--
-- Name: get_poolsizename(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_poolsizename(poolid integer) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT LOWER(poolsize) || ' ' || poolname FROM poolnames
        WHERE poolid=$1;
$_$;


--
-- Name: FUNCTION get_poolsizename(poolid integer); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION public.get_poolsizename(poolid integer) IS '
Returns a pool-size name pair for the pool with the pool ID
';


--
-- Name: insertcustid(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insertcustid(fn character varying, ln character varying, p character varying) RETURNS integer
    LANGUAGE sql
    AS $_$
SELECT cust_id FROM customertable WHERE firstname=$1 AND lastname=$2 AND phone=$3;
$_$;


--
-- Name: mosum(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.mosum(yr integer) RETURNS SETOF public.mosumtype
    LANGUAGE plpgsql
    AS $$

DECLARE
mtot int := 0;
mrow mosumtype;
BEGIN
    FOR mrow in SELECT to_char(mo, 'Mon'),count,0 FROM
    --(SELECT date_part('month',saledate) AS mo, count(*) FROM poolsales WHERE
    (SELECT date_trunc('month',saledate) AS mo, count(*) FROM poolsales WHERE
        date_part('year',saledate)=yr GROUP BY mo ORDER BY mo) a
    LOOP
        mtot := mtot + mrow.mosales;
        mrow.motot := mtot;
        RETURN NEXT mrow;
    END LOOP;
    RETURN;
END;
$$;


--
-- Name: mosumcmp(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.mosumcmp(y1 integer, y2 integer) RETURNS TABLE(mo character varying, m1count integer, m1tot integer, m2count integer, m2tot integer)
    LANGUAGE sql
    AS $_$
SELECT mo,a.mocount AS m1count,a.motot AS m1tot,b.mocount AS m2count,b.motot AS m2tot FROM ((SELECT * FROM msum($1)) a FULL JOIN (SELECT * FROM msum($2)) b USING (monum,mo)) ORDER BY monum;
$_$;


--
-- Name: FUNCTION mosumcmp(y1 integer, y2 integer); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION public.mosumcmp(y1 integer, y2 integer) IS 'RETURNS a summary comparing monthly pool sales between two years.';


--
-- Name: msum(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.msum(yr integer) RETURNS TABLE(mo character varying, monum integer, mocount integer, motot integer)
    LANGUAGE plpgsql
    AS $$
DECLARE
    mtot int := 0;
    mrow record;
BEGIN
    FOR mrow IN SELECT to_char(saledate,'Mon') AS mo_name,
            date_part('month',saledate) AS mo_no,
            count(*) AS mo_count,
            0 AS mo_tot FROM poolsales WHERE date_part('year',saledate)=yr
        GROUP BY mo_no,mo_name ORDER BY mo_no
    LOOP
        mo := mrow.mo_name;
        monum := mrow.mo_no;
        mocount := mrow.mo_count;
        mtot := mtot+mocount;
        motot := mtot;
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: poolsales; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poolsales (
    sale_id integer NOT NULL,
    saledate date,
    status character varying(10),
    p_comment text,
    cust_id integer,
    poolid integer
);


--
-- Name: one_year(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.one_year(yr integer) RETURNS SETOF public.poolsales
    LANGUAGE sql
    AS $$
SELECT * FROM poolsales WHERE date_part('year',saledate)=yr;
$$;


--
-- Name: plpgsql_call_handler(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.plpgsql_call_handler() RETURNS language_handler
    LANGUAGE c
    AS '$libdir/plpgsql', 'plpgsql_call_handler';


--
-- Name: poolsales_by_type(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.poolsales_by_type(recyr integer) RETURNS SETOF public.poolsalesbytype
    LANGUAGE sql
    AS $_$
    SELECT poolsize,poolname, count(*)::integer AS totsales FROM
    (SELECT poolid FROM poolsales WHERE date_part('year',saledate)=$1) AS s
    LEFT JOIN (SELECT * FROM poolnames) AS n
    USING (poolid)
    GROUP BY poolname,poolsize ORDER BY totsales DESC;
$_$;


--
-- Name: FUNCTION poolsales_by_type(recyr integer); Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON FUNCTION public.poolsales_by_type(recyr integer) IS '
Returns a table reporting the total pool sales broken down by pool name
and size.
Passed: $1 = Desired year to check. ';


--
-- Name: customertable; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.customertable (
    cust_id integer NOT NULL,
    lastname character varying(50),
    firstname character varying(50),
    mi character varying(40),
    streetaddr character varying(150),
    city character varying(22),
    state character varying(5),
    zip character varying(12),
    phone character varying(20),
    cell character varying(20)
);


--
-- Name: customertable_cust_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customertable_cust_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customertable_cust_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customertable_cust_id_seq OWNED BY public.customertable.cust_id;


--
-- Name: poolnames; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poolnames (
    poolid integer NOT NULL,
    poolsize character varying(20) NOT NULL,
    poolname character varying(50) NOT NULL,
    is_active boolean DEFAULT true,
    disp_order integer,
    pooltype character(1)
);


--
-- Name: poolnames_poolid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poolnames_poolid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poolnames_poolid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.poolnames_poolid_seq OWNED BY public.poolnames.poolid;


--
-- Name: poolnamesastable; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.poolnamesastable AS
 SELECT ((lpad(initcap((poolnames.poolsize)::text), 10) || ' '::text) || initcap((poolnames.poolname)::text)) AS pool
   FROM public.poolnames
  WHERE (poolnames.is_active IS TRUE)
  ORDER BY poolnames.disp_order;


--
-- Name: poolsales_sale_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poolsales_sale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poolsales_sale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.poolsales_sale_id_seq OWNED BY public.poolsales.sale_id;


--
-- Name: poolsalesfullview; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.poolsalesfullview AS
 SELECT poolsales.sale_id,
    poolsales.saledate,
    poolsales.status,
    poolsales.cust_id,
    customertable.lastname,
    customertable.firstname,
    customertable.mi,
    customertable.streetaddr,
    customertable.city,
    customertable.state,
    customertable.zip,
    customertable.phone,
    customertable.cell,
    poolsales.poolid,
    poolnames.poolsize,
    poolnames.poolname,
    poolnames.pooltype,
    poolsales.p_comment
   FROM ((public.poolsales
     JOIN public.customertable USING (cust_id))
     JOIN public.poolnames USING (poolid));


--
-- Name: VIEW poolsalesfullview; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON VIEW public.poolsalesfullview IS 'Returns a table combining all customer and pool information with the sales info';


--
-- Name: sales_hist; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.sales_hist AS
 SELECT date_part('year'::text, poolsales.saledate) AS yr,
    count(*) AS totals
   FROM public.poolsales
  GROUP BY (date_part('year'::text, poolsales.saledate))
  ORDER BY (date_part('year'::text, poolsales.saledate));


--
-- Name: zz_zipcodes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zz_zipcodes (
    zip character varying(10) NOT NULL,
    state character varying(4) NOT NULL,
    city character varying(30) NOT NULL
);


--
-- Name: customertable cust_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customertable ALTER COLUMN cust_id SET DEFAULT nextval('public.customertable_cust_id_seq'::regclass);


--
-- Name: poolnames poolid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poolnames ALTER COLUMN poolid SET DEFAULT nextval('public.poolnames_poolid_seq'::regclass);


--
-- Name: poolsales sale_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poolsales ALTER COLUMN sale_id SET DEFAULT nextval('public.poolsales_sale_id_seq'::regclass);


--
-- Name: customertable customertable_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customertable
    ADD CONSTRAINT customertable_pkey PRIMARY KEY (cust_id);


--
-- Name: poolnames poolnames_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poolnames
    ADD CONSTRAINT poolnames_pkey PRIMARY KEY (poolid);


--
-- Name: poolsales poolsales_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poolsales
    ADD CONSTRAINT poolsales_pkey PRIMARY KEY (sale_id);


--
-- Name: poolsales poolsales_cust_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poolsales
    ADD CONSTRAINT poolsales_cust_id_fkey FOREIGN KEY (cust_id) REFERENCES public.customertable(cust_id);


--
-- Name: poolsales poolsales_poolid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poolsales
    ADD CONSTRAINT poolsales_poolid_fkey FOREIGN KEY (poolid) REFERENCES public.poolnames(poolid);


--
-- PostgreSQL database dump complete
--

