-- MySQL dump 10.17  Distrib 10.3.18-MariaDB, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: poolstore
-- ------------------------------------------------------
-- Server version	10.3.18-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customertable`
--

DROP TABLE IF EXISTS `customertable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customertable` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `mi` varchar(40) DEFAULT NULL,
  `streetaddr` varchar(150) DEFAULT NULL,
  `city` varchar(22) DEFAULT NULL,
  `state` varchar(5) DEFAULT NULL,
  `zip` varchar(12) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `cell` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2996 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `poolnames`
--

DROP TABLE IF EXISTS `poolnames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poolnames` (
  `poolid` int(11) NOT NULL AUTO_INCREMENT,
  `poolsize` varchar(20) NOT NULL,
  `poolname` varchar(50) NOT NULL,
  `is_active` tinyint(4) DEFAULT 1,
  `disp_order` int(11) DEFAULT NULL,
  `pooltype` char(1) DEFAULT NULL,
  PRIMARY KEY (`poolid`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `poolnamesastable`
--

DROP TABLE IF EXISTS `poolnamesastable`;
/*!50001 DROP VIEW IF EXISTS `poolnamesastable`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `poolnamesastable` (
  `pool` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `poolsales`
--

DROP TABLE IF EXISTS `poolsales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poolsales` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `saledate` date DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `p_comment` longtext DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `poolid` int(11) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `poolsales_cust_id_fkey` (`cust_id`),
  KEY `poolsales_poolid_fkey` (`poolid`),
  CONSTRAINT `poolsales_cust_id_fkey` FOREIGN KEY (`cust_id`) REFERENCES `customertable` (`cust_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `poolsales_poolid_fkey` FOREIGN KEY (`poolid`) REFERENCES `poolnames` (`poolid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3057 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `poolsalesfullview`
--

DROP TABLE IF EXISTS `poolsalesfullview`;
/*!50001 DROP VIEW IF EXISTS `poolsalesfullview`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `poolsalesfullview` (
  `sale_id` tinyint NOT NULL,
  `saledate` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `cust_id` tinyint NOT NULL,
  `lastname` tinyint NOT NULL,
  `firstname` tinyint NOT NULL,
  `mi` tinyint NOT NULL,
  `streetaddr` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `zip` tinyint NOT NULL,
  `phone` tinyint NOT NULL,
  `cell` tinyint NOT NULL,
  `poolid` tinyint NOT NULL,
  `poolsize` tinyint NOT NULL,
  `poolname` tinyint NOT NULL,
  `pooltype` tinyint NOT NULL,
  `p_comment` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `sales_hist`
--

DROP TABLE IF EXISTS `sales_hist`;
/*!50001 DROP VIEW IF EXISTS `sales_hist`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `sales_hist` (
  `yr` tinyint NOT NULL,
  `totals` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `salesrange`
--

DROP TABLE IF EXISTS `salesrange`;
/*!50001 DROP VIEW IF EXISTS `salesrange`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `salesrange` (
  `begin` tinyint NOT NULL,
  `end` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `zz_zipcodes`
--

DROP TABLE IF EXISTS `zz_zipcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_zipcodes` (
  `zip` varchar(10) NOT NULL,
  `state` varchar(4) NOT NULL,
  `city` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'poolstore'
--
/*!50003 DROP FUNCTION IF EXISTS `DATE_TRUNC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp850 */ ;
/*!50003 SET character_set_results = cp850 */ ;
/*!50003 SET collation_connection  = cp850_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `DATE_TRUNC`(field ENUM('microsecond', 'millisecond', 'second', 'minute', 'hour', 'day', 'week', 'month', 'quarter', 'year', 'decade', 'century', 'millennium'), source datetime(6)) RETURNS datetime(6)
    DETERMINISTIC
BEGIN
  IF field IN ('millisecond') THEN SET source = source - INTERVAL MICROSECOND(source) % 1000 MICROSECOND; END IF;
  IF field NOT IN ('microsecond', 'millisecond') THEN SET source = source - INTERVAL MICROSECOND(source) MICROSECOND; END IF;
  IF field NOT IN ('microsecond', 'millisecond', 'second') THEN SET source = source - INTERVAL SECOND(source) SECOND; END IF;
  IF field NOT IN ('microsecond', 'millisecond', 'second', 'minute') THEN SET source = source - INTERVAL MINUTE(source) MINUTE; END IF;
  IF field NOT IN ('microsecond', 'millisecond', 'second', 'minute', 'hour') THEN SET source = source - INTERVAL HOUR(source) HOUR; END IF;
  IF field NOT IN ('microsecond', 'millisecond', 'second', 'minute', 'hour', 'day') THEN SET source = source - INTERVAL DAYOFWEEK(source) - 1 DAY; END IF;
  IF field NOT IN ('microsecond', 'millisecond', 'second', 'minute', 'hour', 'day', 'week') THEN SET source = source - INTERVAL DAY(source) - 1 DAY; END IF;
  IF field IN ('quarter') THEN SET source = source - INTERVAL MONTH(source) % 3 - 1 MONTH; END IF;
  IF field NOT IN ('microsecond', 'millisecond', 'second', 'minute', 'hour', 'week', 'day', 'month', 'quarter') THEN SET source = source - INTERVAL MONTH(source) - 1 MONTH; END IF;

  
  IF field IN ('decade') THEN SET source = source - INTERVAL YEAR(source) % 10 - 1 YEAR; END IF;
  IF field IN ('century') THEN SET source = source - INTERVAL YEAR(source) % 100  - 1 YEAR; END IF;
  IF field IN ('millennium') THEN SET source = source - INTERVAL YEAR(source) % 1000 - 1 YEAR; END IF;
 
  RETURN source;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fullname` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp850 */ ;
/*!50003 SET character_set_results = cp850 */ ;
/*!50003 SET collation_connection  = cp850_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `fullname`(frst text, middle text, last text) RETURNS text CHARSET utf8
    SQL SECURITY INVOKER
BEGIN
  DECLARE fn text DEFAULT '';
  DECLARE tmpnam text;
  DECLARE count INT;

  IF(frst IS NOT NULL) THEN
    SET fn = frst;
  END IF;

  SET count = 2;
  SET tmpnam = middle;

  REPEAT
    IF (tmpnam IS NOT NULL) THEN
      IF CHAR_LENGTH(fn) > 0 THEN
        SET fn = CONCAT(fn, ' ', tmpnam);
      ELSE
        SET fn = tmpnam;
      END IF;
    END IF;

    SET tmpnam = last;
    SET count = count + 1;
  UNTIL count > 3
  END REPEAT;

  RETURN fn;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fullname2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp850 */ ;
/*!50003 SET character_set_results = cp850 */ ;
/*!50003 SET collation_connection  = cp850_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `fullname2`(ln text, fn text) RETURNS text CHARSET utf8
    SQL SECURITY INVOKER
RETURN (fullname(ln, NULL, fn)) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_poolid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp850 */ ;
/*!50003 SET character_set_results = cp850 */ ;
/*!50003 SET collation_connection  = cp850_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `get_poolid`(siz text, nam text) RETURNS int(11)
RETURN (SELECT poolid FROM poolnames WHERE poolsize=siz AND poolname=nam) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_poolsizename` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `get_poolsizename`(pid int) RETURNS varchar(80) CHARSET utf8mb4
BEGIN
DECLARE ps varchar(80);
SELECT CONCAT(LOWER(poolsize),' ',poolname) INTO ps FROM poolnames where poolid=pid;
return ps;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mosum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `mosum`(yr integer)
BEGIN
    DECLARE done int DEFAULT FALSE;
    DECLARE mnth DATE;
    DECLARE mosales INT;
    DECLARE mtot int DEFAULT 0;
    DECLARE crs CURSOR FOR
        SELECT DATE(DATE_FORMAT(saledate, '%Y-%m-01')) AS mo, count(*)
            FROM poolsales WHERE YEAR(saledate)=yr GROUP BY mo ORDER BY mo;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DROP TABLE IF EXISTS mosumtbl;
    CREATE TABLE mosumtbl (mo varchar(5), mnthsales INT, motot INT);
    OPEN crs;
    ms_loop: LOOP
        FETCH crs INTO mnth, mosales;
        IF done THEN
            LEAVE ms_loop;
        END IF;
        SET mtot = mtot + mosales;
        INSERT INTO mosumtbl (mo,mnthsales,motot) VALUES
        (DATE_FORMAT(mnth,'%b'),mosales,mtot);
    END LOOP;
    CLOSE crs;
    SELECT * FROM mosumtbl;
    DROP TABLE mosumtbl;
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mosumcmp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `mosumcmp`(y1 integer, y2 integer)
BEGIN
  DECLARE mnth varchar(5);
  DECLARE m1c,m2c INT;
  DECLARE m1ttl INT DEFAULT 0;
  DECLARE m2ttl INT DEFAULT 0;
  DECLARE done int DEFAULT FALSE;
  DECLARE c1 CURSOR FOR SELECT mo, m1coun, m2coun
      FROM ((SELECT a.ord,a.mo,a.m1coun,b.m2coun
            FROM (SELECT MONTH(saledate) AS ord,
                DATE_FORMAT(saledate,'%b') AS mo,count(*) AS m1coun
                FROM poolsales WHERE YEAR(saledate)=y1 GROUP BY mo) AS a
            LEFT JOIN (SELECT MONTH(saledate) AS ord,
                DATE_FORMAT(saledate,'%b') AS mo, count(*) AS m2coun
                FROM poolsales WHERE YEAR(saledate)=y2 GROUP BY mo) AS b
            USING (ord,mo))
        UNION
        SELECT b.ord,b.mo,a.m1coun,b.m2coun
        FROM (SELECT MONTH(saledate) AS ord,
            DATE_FORMAT(saledate,'%b') AS mo,count(*) AS m1coun
            FROM poolsales WHERE YEAR(saledate)=y1 GROUP BY mo) AS a
        RIGHT JOIN (SELECT MONTH(saledate) AS ord,
        DATE_FORMAT(saledate,'%b') AS mo, count(*) AS m2coun
        FROM poolsales WHERE YEAR(saledate)=y2 GROUP BY mo) AS b
    USING (ord,mo) WHERE m1coun IS NULL) AS j ORDER BY ord;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DROP TABLE IF EXISTS cmptbl;
    CREATE TABLE cmptbl (mo varchar(5), m1count INT,m1tot INT,m2count INT,
        m2tot INT);
    OPEN c1;
    
    cmploop: LOOP
    FETCH c1 INTO mnth,m1c,m2c;
    
    IF done THEN
        LEAVE cmploop;
    END IF;

    IF m1c IS NOT NULL THEN
        SET m1ttl = m1ttl + coalesce(m1c,0);
    END IF;

    if m2c IS NOT NULL THEN
        SET m2ttl = m2ttl + coalesce(m2c,0);
    END IF;

    INSERT INTO cmptbl (mo,m1count,m1tot,m2count,m2tot)
        VALUES (mnth,m1c,m1ttl,m2c,m2ttl);
  END LOOP;

  CLOSE c1;
  SELECT * FROM cmptbl;
  DROP TABLE cmptbl;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `one_year` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp850 */ ;
/*!50003 SET character_set_results = cp850 */ ;
/*!50003 SET collation_connection  = cp850_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `one_year`(yr integer)
SELECT * FROM poolsales WHERE year(saledate)=yr ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `poolsales_by_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp850 */ ;
/*!50003 SET character_set_results = cp850 */ ;
/*!50003 SET collation_connection  = cp850_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `poolsales_by_type`(recyr int)
SELECT poolsize,poolname, count(*) AS totsales FROM
    (SELECT poolid FROM poolsales WHERE year(saledate)=recyr) AS s
    LEFT OUTER JOIN (SELECT * FROM poolnames) AS n
    USING (poolid)
    GROUP BY poolname,poolsize ORDER BY totsales DESC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `poolnamesastable`
--

/*!50001 DROP TABLE IF EXISTS `poolnamesastable`*/;
/*!50001 DROP VIEW IF EXISTS `poolnamesastable`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `poolnamesastable` AS select concat(lpad(concat(ucase(left(`poolnames`.`poolsize`,1)),lcase(substr(`poolnames`.`poolsize`,2))),10,' '),' ',concat(ucase(left(`poolnames`.`poolname`,1)),lcase(substr(`poolnames`.`poolname`,2)))) AS `pool` from `poolnames` where `poolnames`.`is_active` is true order by `poolnames`.`disp_order` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `poolsalesfullview`
--

/*!50001 DROP TABLE IF EXISTS `poolsalesfullview`*/;
/*!50001 DROP VIEW IF EXISTS `poolsalesfullview`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `poolsalesfullview` AS select `poolsales`.`sale_id` AS `sale_id`,`poolsales`.`saledate` AS `saledate`,`poolsales`.`status` AS `status`,`poolsales`.`cust_id` AS `cust_id`,`customertable`.`lastname` AS `lastname`,`customertable`.`firstname` AS `firstname`,`customertable`.`mi` AS `mi`,`customertable`.`streetaddr` AS `streetaddr`,`customertable`.`city` AS `city`,`customertable`.`state` AS `state`,`customertable`.`zip` AS `zip`,`customertable`.`phone` AS `phone`,`customertable`.`cell` AS `cell`,`poolsales`.`poolid` AS `poolid`,`poolnames`.`poolsize` AS `poolsize`,`poolnames`.`poolname` AS `poolname`,`poolnames`.`pooltype` AS `pooltype`,`poolsales`.`p_comment` AS `p_comment` from ((`poolsales` join `customertable` on(`poolsales`.`cust_id` = `customertable`.`cust_id`)) join `poolnames` on(`poolsales`.`poolid` = `poolnames`.`poolid`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `sales_hist`
--

/*!50001 DROP TABLE IF EXISTS `sales_hist`*/;
/*!50001 DROP VIEW IF EXISTS `sales_hist`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `sales_hist` AS select year(`poolsales`.`saledate`) AS `yr`,count(0) AS `totals` from `poolsales` group by year(`poolsales`.`saledate`) order by year(`poolsales`.`saledate`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `salesrange`
--

/*!50001 DROP TABLE IF EXISTS `salesrange`*/;
/*!50001 DROP VIEW IF EXISTS `salesrange`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `salesrange` AS select min(year(`poolsales`.`saledate`)) AS `begin`,max(year(`poolsales`.`saledate`)) AS `end` from `poolsales` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-30 18:55:58
